# README #
The purpose of this README is to descripe steps are necessary to get MVU3 application up and running.

### Hardware ###

Board: MVU3 PW00004

Tools: KiCad 

http://kicad-pcb.org/

## Firmware ##
### Development tool: ###

MPLAB X IDE V6.00

https://www.microchip.com/mplab/mplaDeb-x-ide

Windows and linux

### Compiler ###

XC8 Compiler V2.31

Pack PIC16F1xxxx_DFP V1.5.133

https://www.microchip.com/mplab/compilers

Free version

### Debugger ###

MPLAB ICD3 Debugger

Needed only for debugging and firmware update if bootloader is not preprogrammed

### Settings ###

Application:

- Extra loadables: MVU3_Bootloader.X

- C standard: C90

- Link in C Library: C90

- Memory model: 24 bit

- Size of float: 24 bit

- Which are to fill: N/A

- Codeofset: 0x600

Bootloader

- Extra loadables:

- C standard: C90

- Link in C Library: C90

- Memory model: 24 bit

- Size of float: 24 bit

- ROM Ranges: 0-5FF

- Which are to fill: N/A

- Codeofset: 


### How to compile ###

Open MVU3_Application.X project

Compile project (2nd click project and select Build)

If build was succesful there should be text "Build succesful" in output window

Output can be found in folder: /firmware/MVU3_Application.X/dist/default/production

MVU3_Application.X.production.hex can be used for update made with bootloader

MVU3_Application.X.production.unified.hex has also bootloader. Update should be done only by programmer (ICD3)







