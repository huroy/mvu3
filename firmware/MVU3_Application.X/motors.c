/*
 * Updated by Tapio Yliniemi 22.11.2021
 * */

#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/LINDrivers/lin_app.h"
#include <xc.h>
#include "motors.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/LINDrivers/lin_hardware.h"
#include "valves.h"
#include "full_bridge.h"
#include "eeprom_addresses.h"

uint16_t globalMotorTimeOut = GLOBAL_MOTOR_TIMEOUT;
uint16_t listenReedticksTime = LISTEN_REED_TICKS_TIME;
uint16_t sendDataTimer = 0;

MotorsStatus motors[3];

void motorMove(uint8_t motorNumber, uint8_t direction, uint8_t speed, bool useLimits);
void motorGoToPosition(uint8_t motorNumber, uint16_t value, uint8_t speed, bool enableInterrupts);
int get_position_data(uint8_t high, uint8_t low);
void moveMotorsDown(int motorNumber, int offset);
void sendPositionDataUp(uint8_t high, uint8_t low);
uint16_t motorGetOffset(uint8_t motor);
void updatePositionResponse(uint8_t motor);
bool detectUpMotors(uint8_t motorNumber);
bool detectDownMotors(uint8_t motorNumber);


void motorsMoveDown(){
    //Move motors to offset position and reset position
    uint16_t offset;
    uint8_t motor_index;
    
    for(motor_index=0;motor_index<3;++motor_index){
        offset = motorGetOffset(motor_index);      
        moveMotorsDown(motor_index, 9999);
        moveMotorsDown(motor_index, 9999);
        motorGoToPosition(motor_index, offset, RESET_SPEED, false);
        motors[motor_index].currentHeight = 0;
        updatePositionResponse(motor_index);
    }
}

void motorOneGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts){
    motorGoToPosition(0, value, speed, enableInterrupts);
    
    if(motors[0].currentHeight != value){
        motorGoToPosition(0, value, speed, enableInterrupts);        
    }
}

void motorTwoGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts){
        motorGoToPosition(1, value, speed, enableInterrupts);   
}

void motorThreeGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts){
    motorGoToPosition(2, value, speed, enableInterrupts);
}



uint8_t motorGetSensorValue(uint8_t motor){
    uint8_t returnValue = 0;    
    switch(motor){
        case 0:
            returnValue = MSENS1_GetValue();
            break;
        case 1: 
            returnValue = MSENS2_GetValue();
            break;
        case 2:
            returnValue = MSENS3_GetValue();
            break;
        default:
            break;                        
    }
    return returnValue;
}
int16_t motorGetMaximumHeight(uint8_t motor){
    int16_t returnValue = 0;    
    switch(motor){
        case 0:
            returnValue = get_position_data(ADDRESS_MOTOR_1_MAX_HIGH, ADDRESS_MOTOR_1_MAX_LOW);
            break;
        case 1: 
            returnValue = get_position_data(ADDRESS_MOTOR_2_MAX_HIGH, ADDRESS_MOTOR_2_MAX_LOW);
            break;
        case 2:
            returnValue = get_position_data(ADDRESS_MOTOR_3_MAX_HIGH, ADDRESS_MOTOR_3_MAX_LOW);
            break;
        default:
            break;                        
    }
    return returnValue;
}

uint16_t motorGetOffset(uint8_t motor){
    uint16_t returnValue = 0;    
    switch(motor){
        case 0:
            returnValue = EEPROM_READ(ADDRESS_MOTOR_1_OFFSET);
            break;
        case 1: 
            returnValue = EEPROM_READ(ADDRESS_MOTOR_2_OFFSET);
            break;
        case 2:
            returnValue = EEPROM_READ(ADDRESS_MOTOR_3_OFFSET);
            break;
        default:
            break;                        
    }
    return returnValue;
}

void motorSetOffset(uint8_t motor, uint8_t offset){   
    switch(motor){
        case 0:
            EEPROM_WRITE(ADDRESS_MOTOR_1_OFFSET, offset);
            break;
        case 1: 
            EEPROM_WRITE(ADDRESS_MOTOR_2_OFFSET, offset);
            break;
        case 2:
            EEPROM_WRITE(ADDRESS_MOTOR_3_OFFSET, offset);
            break;
        default:
            break;                        
    }
}


void motorSetMaxHeight(uint8_t motor, int position) {

    uint8_t lowByte = position & 0xff; // measure maximum position
    uint8_t highByte = (position >> 8);

    switch(motor){
        case 0:
            EEPROM_WRITE(ADDRESS_MOTOR_1_MAX_HIGH, highByte); // save measured maximum to EEPROM
            EEPROM_WRITE(ADDRESS_MOTOR_1_MAX_LOW, lowByte);
            break;
        case 1: 
            EEPROM_WRITE(ADDRESS_MOTOR_2_MAX_HIGH, highByte); // save measured maximum to EEPROM
            EEPROM_WRITE(ADDRESS_MOTOR_2_MAX_LOW, lowByte);
            break;
        case 2:
            EEPROM_WRITE(ADDRESS_MOTOR_3_MAX_HIGH, highByte); // save measured maximum to EEPROM
            EEPROM_WRITE(ADDRESS_MOTOR_3_MAX_LOW, lowByte);
            break;
        default:
            break;                        
    }
}

void updatePositionResponse(uint8_t motor){
    uint16_t position = 0;
    if(motors[motor].currentHeight > 0){
        position = motors[motor].currentHeight;
    }
    MOTORPOSITION_Data[0+motor*2] = (uint8_t) (position >> 8);
    MOTORPOSITION_Data[1+motor*2] = (uint8_t) position;
}

void increaseMotorPosition(uint8_t motorNumber){
    motors[motorNumber].currentHeight++;
}

void decreaseMotorPosition(uint8_t motorNumber){
    motors[motorNumber].currentHeight--;
}

void sendMotorPositionData(int16_t data) {
    uint16_t position = 0;
    if(data > 0){
        position = data;
    }
    sendDataTimer++;
    if (sendDataTimer > 100) {
        uint8_t low = position & 0xff;
        uint8_t high = (position >> 8);
        sendPositionDataUp(high, low);
        sendDataTimer = 0;
    }
}

/*
 motorOneMove is used to "free run motors" until something is sent to LIN bus.
 */
void motorOneMove(uint8_t direction, uint8_t speed, bool useLimits){
    
    motorMove(0, direction, speed, useLimits);
    
}

void motorTwoMove(uint8_t direction, uint8_t speed, bool useLimits){
    
    motorMove(1, direction, speed, useLimits);
    
}

void motorThreeMove(uint8_t direction, uint8_t speed, bool useLimits){
    
    motorMove(2, direction, speed, useLimits);
}
void motorMove(uint8_t motorNumber, uint8_t direction, uint8_t speed, bool useLimits){

    // uint16_t motorOffSet = 0;
    
    uint16_t motorTimeOut = 0;
    uint16_t speedLimit;
    uint16_t TargetSpeedLimit;
    bool breaking = false;
    uint32_t topBrakeValue;
    uint32_t bottomBrakeValue;
    int16_t MotorMaximumHeight;
    uint8_t oldValue;
    uint8_t kick_start_counter = MOTOR_RUN_DETECTION_ROUNDS;
    
    sendDataTimer = 0;
    setMotorDutyValue(0);
    
    if(motorNumber == 0) selectedOutputBridge=bridge1;
    if(motorNumber == 1) selectedOutputBridge=bridge2;
    if(motorNumber == 2) selectedOutputBridge=bridge3;
    
    MotorMaximumHeight = MAXIMUM_HEIGHT;
    // speedLimit = ((speed + 1) * 4) - 1;;
    
    TargetSpeedLimit = ((speed + 1) * 4) - 1;
    speedLimit = MOTOR_START_DUTY_VALUE;
    
    enableOutputs();
    
    if(useLimits){
         MotorMaximumHeight = motorGetMaximumHeight(motorNumber);
    }
    
    topBrakeValue = (MotorMaximumHeight * TOP_BRAKING_POINT) / 100;
    if(topBrakeValue < MINIMUM_TOP_BRAKING_POINT) topBrakeValue = MINIMUM_TOP_BRAKING_POINT;

    bottomBrakeValue = (MotorMaximumHeight * BOTTOM_BRAKING_POINT) / 100;
    if(bottomBrakeValue > MAXIMUM_BOTTOM_BRAKING_POINT) bottomBrakeValue = MAXIMUM_BOTTOM_BRAKING_POINT;
     
    if (direction == 1) {

        oldValue = motorGetSensorValue(motorNumber);
        
        moveDirectionMotor = UP;
        //Acceleration phase
        while (motors[motorNumber].currentHeight < MotorMaximumHeight && !LIN_CANCEL) {
            if ( motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                increaseMotorPosition(motorNumber);
                
                if(kick_start_counter) kick_start_counter--;
                else{
                        kickStart = false;
                        speedLimit = TargetSpeedLimit;
                    }
                
                motorTimeOut = 0;
                oldValue = 1;
            }
            if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                motorTimeOut = 0;
                oldValue = 0;
            } else
                motorTimeOut++;

            if(motors[motorNumber].currentHeight > (MotorMaximumHeight - (uint16_t)topBrakeValue)){
                breaking = true;
            }
            if(breaking == false){
                if (motorDutyValue < speedLimit) {
                    motorDutyValue += DUTY_INCREASE_VALUE;
                    setMotorDutyValue(motorDutyValue);
                }
            } else {
                if(motorDutyValue > MINIMUM_DUTY_VALUE){
                    motorDutyValue -= DUTY_DECREASE_VALUE;
                    setMotorDutyValue(motorDutyValue);
                } else {
                    if(motorDutyValue < MINIMUM_DUTY_VALUE) {
                        motorDutyValue += DUTY_INCREASE_VALUE;
                        setMotorDutyValue(motorDutyValue);
                    }
                }
            }

            sendMotorPositionData(motors[motorNumber].currentHeight);

            if (motorTimeOut > globalMotorTimeOut || bridgeStatus == OVERCURRENT) { // DEFAULT 500
                __delay_ms(200);
                setMotorDutyValue(0);
                break;
            }
            __delay_ms(1);
        }
        //Breaking phase
        //Stop the motor immediately if we are already breaking. Otherwise we might go over/below max/min values
        if(breaking){
            setMotorDutyValue(0);
        }
        if (motorDutyValue > 0) {
            while (motorDutyValue > 0) {
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    increaseMotorPosition(motorNumber);
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                }

                sendMotorPositionData(motors[motorNumber].currentHeight);
                __delay_us(100);
                motorDutyValue--;
                if(motorDutyValue < MINIMUM_DUTY_VALUE) motorDutyValue = 0;
                setMotorDutyValue(motorDutyValue);
            }

            for (int i = 0; i < listenReedticksTime; i++) { // LISTEN FOR REED IF ANY TICKS DETECTED AFTER STOP
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    increaseMotorPosition(motorNumber);
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                }
                __delay_ms(1);
            }
        }

        moveDirectionMotor = DOWN;
        setMotorDutyValue(0);
        __delay_ms(20);
    }

    if (direction == 0) {
        oldValue = motorGetSensorValue(motorNumber);
        moveDirectionMotor = DOWN;
            
        //Acceleration phase
        while (!LIN_CANCEL && motors[motorNumber].currentHeight > 0) {
            
            if(motors[motorNumber].currentHeight < (uint16_t)bottomBrakeValue){
                breaking = true;
            }
            if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                decreaseMotorPosition(motorNumber);
                
                if(kick_start_counter) kick_start_counter--;
                else{
                        kickStart = false;
                        speedLimit = TargetSpeedLimit;
                    }
                
                motorTimeOut = 0;
                oldValue = 1;
            }
            if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                motorTimeOut = 0;
                oldValue = 0;
            } else
                motorTimeOut++;
            
            if(breaking == false){
                if (motorDutyValue < speedLimit) {
                    motorDutyValue += DUTY_INCREASE_VALUE;
                    setMotorDutyValue(motorDutyValue);
                }
            } else {
                if(motorDutyValue > MINIMUM_DUTY_VALUE){
                    motorDutyValue-= DUTY_DECREASE_VALUE;
                    setMotorDutyValue(motorDutyValue);
                } else {
                    if(motorDutyValue < MINIMUM_DUTY_VALUE) {
                         motorDutyValue += DUTY_INCREASE_VALUE;
                        // motorDutyValue++;
                        setMotorDutyValue(motorDutyValue);
                    }
                }
            }

            sendMotorPositionData(motors[motorNumber].currentHeight);

            if (motorTimeOut > globalMotorTimeOut || bridgeStatus == OVERCURRENT) { // DEFAULT 500
                __delay_ms(200);
                setMotorDutyValue(0);
                break;
            }
            __delay_ms(1);
        }
        //Stop the motor immediately if we are already breaking. Otherwise we might go over/below max/min values
        if(breaking){

            setMotorDutyValue(0);
        }
        if (motorDutyValue > 0) {
 
            while (motorDutyValue > 0) {        
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    decreaseMotorPosition(motorNumber);
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                }

                sendMotorPositionData(motors[motorNumber].currentHeight);
                __delay_us(100);
                motorDutyValue--;
                if(motorDutyValue < MINIMUM_DUTY_VALUE) motorDutyValue = 0;
                setMotorDutyValue(motorDutyValue);
            }

            for (int i = 0; i < listenReedticksTime; i++) {
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    decreaseMotorPosition(motorNumber);
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                }
                __delay_ms(1);
            }
        }
        moveDirectionMotor = UP;
        setMotorDutyValue(0);
        __delay_ms(20);
    }
    disableOutputs();
    
    updatePositionResponse(motorNumber);
}

/*
 * Reads position data from eeprom, used only to retrieve saved maximum values
 */
int get_position_data(uint8_t high, uint8_t low) {
    uint16_t hi = EEPROM_READ(high);
    uint16_t lo = EEPROM_READ(low);
    uint16_t position = (hi << 8) | lo;
    return position;
}

void updateSlaveData() {

    for (uint16_t i = 7; i < 13; i++) {
        DEBUG_Data[i - 7] = EEPROM_READ(i);
    }


}


/*
 * detectMotors will try to move requested motor for little and return if motor is attached or not
 */
bool detectUpMotors(uint8_t motorNumber) {
    
    int8_t oldValue = 0;
    uint16_t motorTimeOut = 0;
    uint16_t i;
    uint8_t kick_start_counter = MOTOR_RUN_DETECTION_ROUNDS;

    setMotorDutyValue(0);
    moveDirectionMotor=UP;
    enableOutputs();
    
    if(motorNumber == 0) selectedOutputBridge=bridge1;
    if(motorNumber == 1) selectedOutputBridge=bridge2;
    if(motorNumber == 2) selectedOutputBridge=bridge3;    
    
     for (i = 0; i < MOTOR_DETECTION_RUN_TIME; i++) { // start to move motor for short time
            if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                if(kick_start_counter) kick_start_counter--;
                else{
                        kickStart = false;
                    }
                
                motorTimeOut = 0;
                oldValue = 1;
            }
            if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                motorTimeOut = 0;
                oldValue = 0;
            } else motorTimeOut++;
            
            __delay_ms(1);
            
            if (motorDutyValue < MOTOR_DETECT_DUTY_VALUE) {
                motorDutyValue += DETECT_DUTY_INCREASE_VALUE;
                setMotorDutyValue(motorDutyValue);
            }
            if (motorTimeOut > MOTOR_DETECTION_TIME || bridgeStatus == OVERCURRENT) { // if we timeout return false
                setMotorDutyValue(0);
                disableOutputs();
                return false;
            }
        }
        setMotorDutyValue(0);
        __delay_ms(200);
        moveDirectionMotor=DOWN;
        disableOutputs();
        return true; // return true if timeout did not happen = we can assume motor is attached

}

bool detectMotors(uint8_t motorNumber) {
    
    motorNumber--;  
    if(detectUpMotors(motorNumber)) return true;
    if(detectDownMotors(motorNumber)) return true;
    return false;
    
}

/*
 * detectMotors will try to move requested motor for little and return if motor is attached or not
 */
bool detectDownMotors(uint8_t motorNumber) {

    
    int8_t oldValue = 0;
    uint16_t motorTimeOut = 0;
    uint16_t i;
    uint8_t kick_start_counter = MOTOR_RUN_DETECTION_ROUNDS;    

    setMotorDutyValue(0);
    moveDirectionMotor=DOWN;
    enableOutputs();
    
    if(motorNumber == 0) selectedOutputBridge=bridge1;
    if(motorNumber == 1) selectedOutputBridge=bridge2;
    if(motorNumber == 2) selectedOutputBridge=bridge3;    
    
     for (i = 0; i < MOTOR_DETECTION_RUN_TIME; i++) { // start to move motor for short time
            if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                if(kick_start_counter) kick_start_counter--;
                else{
                        kickStart = false;
                    }
                
                motorTimeOut = 0;
                oldValue = 1;
            }
            if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                motorTimeOut = 0;
                oldValue = 0;
            } else motorTimeOut++;
            
            __delay_ms(1);
            
            if (motorDutyValue < MOTOR_DETECT_DUTY_VALUE) {
                motorDutyValue += DETECT_DUTY_INCREASE_VALUE;
                setMotorDutyValue(motorDutyValue);
            }
            if (motorTimeOut > MOTOR_DETECTION_TIME || bridgeStatus == OVERCURRENT) { // if we timeout return false
                setMotorDutyValue(0);
                disableOutputs();
                return false;
            }
        }
        setMotorDutyValue(0);
        __delay_ms(200);
        moveDirectionMotor=DOWN;
        disableOutputs();
        return true; // return true if timeout did not happen = we can assume motor is attached

}



void moveMotorsDown(int motorNumber, int offset) {
    motors[motorNumber].currentHeight = 9999;

    motorGoToPosition(motorNumber, 0, RESET_SPEED, false);
    motors[motorNumber].currentHeight = 0;
}



void sendPositionDataUp(uint8_t high, uint8_t low) {
    pauseCancelWait();
   
    EUSART1_Write(high);
    EUSART1_Write(low);
    __delay_ms(1);
    continueCancelWait();
}

void checkManualInputs() {
    int manualModeCounter = 0;

    if (PLEX0_GetValue() == 1 && PLEX1_GetValue() == 0 && PLEX2_GetValue() == 0) {
        VALVE0_SetHigh();
    } else {
        VALVE0_SetLow();
    }

    if (PLEX0_GetValue() == 0 && PLEX1_GetValue() == 1 && PLEX2_GetValue() == 0) {
        VALVE1_SetHigh();
    } else {
        VALVE1_SetLow();
    }

    if (PLEX0_GetValue() == 1 && PLEX1_GetValue() == 1 && PLEX2_GetValue() == 0) {
        motorOneGoToPosition(0x00, 0xff, false);
    }
    if (PLEX0_GetValue() == 0 && PLEX1_GetValue() == 0 && PLEX2_GetValue() == 1) {
        motorOneGoToPosition(0x0A, 0xff, false);
    }

}

void resetAllMotors(uint8_t * offsets) {
    uint8_t motor_number, motor_index;
    uint16_t maxHeight;
    uint16_t minOffset, maxOffset;
    uint16_t onePercent;
    
    for(motor_index = 0;motor_index < 3;++motor_index){
        motor_number = motor_index +1;
        motorSetMaxHeight(motor_index, DEFAULT_MOTOR_MAX_VALUE);
        if (detectMotors(motor_number)) {
            
            minOffset = offsets[0+motor_index];
            maxOffset = offsets[3+motor_index];

            moveMotorsDown(motor_index, 9999); // Move motor x to bottom.
            // motorGoToPosition(motor_index, 32000, 200, false); // request motor x to go maximum position
            motorGoToPosition(motor_index, 32000, RESET_SPEED, false); // request motor one to go maximum position
            // onePercent = (float) motors[motor_index].currentHeight / 100;
            // onePercent = motors[motor_index].currentHeight
            minOffset = motors[motor_index].currentHeight * minOffset / 100;
            maxOffset = motors[motor_index].currentHeight * maxOffset / 100;
            maxHeight = motors[motor_index].currentHeight - minOffset - maxOffset;           
            motorSetMaxHeight(motor_index, maxHeight);
            motorSetOffset(motor_index, minOffset);
            __delay_ms(10);
            moveMotorsDown(motor_index, 9999); // move motors back to bottom
            motorGoToPosition(motor_index, minOffset, RESET_SPEED, false); // request motor x to go maximum position 
            motors[motor_index].currentHeight = 0; // after offset set, define 0 point
            updatePositionResponse(motor_index);
        } else
            __delay_ms(50);
    }
    updateSlaveData();
    EEPROM_WRITE(ADDRESS_SETUP_DONE, 1); // At the end toggle flag that factory setup is done
}


void motorGoToPosition(uint8_t motorNumber, uint16_t position, uint8_t speed, bool enableInterrupts) {

    // uint16_t motorDutyValue = 0;
    
    uint16_t motorTimeOut = 0;
    uint16_t speedLimit;
    uint16_t TargetSpeedLimit;
    bool breaking = false;
    uint16_t MotorMaximumHeight;
    int16_t value = 0;
    uint8_t kick_start_counter = MOTOR_RUN_DETECTION_ROUNDS;
    uint32_t topBrakeValue;
    uint32_t bottomBrakeValue;
    
    int8_t oldValue;
    
    setMotorDutyValue(0);
    if(motorNumber == 0) selectedOutputBridge=bridge1;
    if(motorNumber == 1) selectedOutputBridge=bridge2;
    if(motorNumber == 2) selectedOutputBridge=bridge3;
    
    TargetSpeedLimit = ((speed + 1) * 4) - 1;
    speedLimit = MOTOR_START_DUTY_VALUE;
    
    enableOutputs();

    if(position > DEFAULT_MOTOR_MAX_VALUE){
        value = DEFAULT_MOTOR_MAX_VALUE;
    } else {
        value = position;
    }
    
    topBrakeValue = (value * TOP_BRAKING_POINT) / 100;
    if(topBrakeValue < MINIMUM_TOP_BRAKING_POINT) topBrakeValue = MINIMUM_TOP_BRAKING_POINT;

    MotorMaximumHeight = motorGetMaximumHeight(motorNumber); // Read maximum positions from EEPROM, set by FACTORY setup
    if (value > MotorMaximumHeight) // don't accept over maximum values
        value = MotorMaximumHeight;
    
    bottomBrakeValue = (MotorMaximumHeight * BOTTOM_BRAKING_POINT) / 100;
    if(bottomBrakeValue > MAXIMUM_BOTTOM_BRAKING_POINT) bottomBrakeValue = MAXIMUM_BOTTOM_BRAKING_POINT;

    if(value != motors[motorNumber].currentHeight){
        if (value > motors[motorNumber].currentHeight) // If height is more than current height move up.
        { 
            oldValue = motorGetSensorValue(motorNumber); // read current status of "reed sampling input"
            moveDirectionMotor = UP; // define direction of motor

            while (value != motors[motorNumber].currentHeight && (!LIN_CANCEL || enableInterrupts == 0)) { // do until destination reached
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) { // compare is the reed sampling state changed, if so we add to current height.
                    motors[motorNumber].currentHeight++;
                    
                    if(kick_start_counter) kick_start_counter--;
                    else{
                        kickStart = false;
                        speedLimit = TargetSpeedLimit;
                    }
                    
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                } else
                    motorTimeOut++;

                if (motorDutyValue < speedLimit && breaking == false) { // Add more speed to motor, soft starting
                    setMotorDutyValue(motorDutyValue + DUTY_INCREASE_VALUE);
                // } else if ((motors[motorNumber].currentHeight > (value - (uint16_t)brakeValue)) && (motorDutyValue > MINIMUM_DUTY_VALUE)) {
                }
                if ((motors[motorNumber].currentHeight > (value - (uint16_t)topBrakeValue)) && (motorDutyValue > MINIMUM_DUTY_VALUE)) {
                    setMotorDutyValue(motorDutyValue - DUTY_DECREASE_VALUE);
                    breaking = true;
                }

                if (motorTimeOut > GLOBAL_MOTOR_TIMEOUT || bridgeStatus == OVERCURRENT) { // this will timeout the function if problems encountered
                    // __delay_ms(200);
                   setMotorDutyValue(0);
                    break;
                }
                __delay_ms(1);
            }
            
            moveDirectionMotor = DOWN; // change direction of motor at the end to "free wheel" energy from motor.
            setMotorDutyValue(0);
            
            //Listen for position shortly to prevent drifting
            for (int i = 0; i < listenReedticksTime; i++) {
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    increaseMotorPosition(motorNumber);
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    oldValue = 0;
                }
                __delay_ms(1);
            }
              
        }


        else if (value < motors[motorNumber].currentHeight) { // Same things as bigger than current value but to different direction.

            oldValue = motorGetSensorValue(motorNumber);
            moveDirectionMotor = DOWN;

            while (value != motors[motorNumber].currentHeight && (!LIN_CANCEL || enableInterrupts == 0)) {
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    motors[motorNumber].currentHeight--;
                    
                    if(kick_start_counter) kick_start_counter--;
                    else{
                        kickStart = false;
                        speedLimit = TargetSpeedLimit;
                    }
                    
                    motorTimeOut = 0;
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    motorTimeOut = 0;
                    oldValue = 0;
                } else
                    motorTimeOut++;

                if (motorDutyValue < speedLimit && breaking == false) {
                    setMotorDutyValue(motorDutyValue + DUTY_INCREASE_VALUE);
                } 
                // if (motors[motorNumber].currentHeight < (uint16_t)brakeValue && motorDutyValue > MINIMUM_DUTY_VALUE) {
                if (motors[motorNumber].currentHeight < bottomBrakeValue && motorDutyValue > MINIMUM_DUTY_VALUE) {
                    setMotorDutyValue(motorDutyValue - DUTY_DECREASE_VALUE);
                    breaking = true;
                }

                if (motorTimeOut > GLOBAL_MOTOR_TIMEOUT  || bridgeStatus == OVERCURRENT) {
                    // __delay_ms(200);
                    setMotorDutyValue(0);
                   break;
                }
                __delay_ms(1);
            }
            moveDirectionMotor = UP;
            setMotorDutyValue(0);
            //Listen for position shortly to prevent drifting
            for (int i = 0; i < listenReedticksTime; i++) {
                if (motorGetSensorValue(motorNumber) == 1 && oldValue == 0) {
                    decreaseMotorPosition(motorNumber);
                    oldValue = 1;
                }
                if (motorGetSensorValue(motorNumber) == 0 && oldValue == 1) {
                    oldValue = 0;
                }
                __delay_ms(1);
            }                      
        }
    } else {
        __delay_ms(200);
    }
    
    updatePositionResponse(motorNumber);
    disableOutputs(); // Disable motor outputs at the end.
    
    EUSART1_Write(0xff); // IMPORTANT: Tells to Android app adjusting is ready by sending 0xFF ??
    
}

void setMotorDutyValue(int16_t value){
    motorDutyValue = PWM2_LoadDutyValue(value);
    // motorDutyValue = value;
}