/**
  Main Source File

  Author:
  Tapio Yliniemi
  
  Date: 
  23.09.2022
 
  Company:
  Mikronix Oy

  File Name:
    main.c

  Summary:
    
*/



#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/LINDrivers/lin_slave.h"

#include "valves.h"
#include "full_bridge.h"
#include "motors.h"


/*
                         Main application
 */
void main(void)
{
    
 
    // initialize the device
    SYSTEM_Initialize();
    
    /* Moved to MVU boot section*/
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    // INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    // INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
     
    while (1)
    {
             
        // Add your application code
        LIN_handler();
        valveWatchDogHandlder();
    }
}
/**
 End of File
*/