/* 
 * Updated by Tapio Yliniemi 22.11.2021
 * */

#include <xc.h>
#include "mcc_generated_files/pin_manager.h"
#include "valves.h"
// #include "mvuBoard1.h"
#include <stdbool.h>
#include "mcc_generated_files/mcc.h"

//0 = Normally closed
//1 = Normally open
uint8_t valvePolarity = 0x00;

uint8_t watchDogValve;
bool valveTimeout;

static void setValveHigh(uint8_t valveNumber);
static void setValveLow(uint8_t valveNumber);


uint8_t timeoutcounter = 0;

bool valveInitCompleted = false;
#define VALVE_TIMEOUT 2


void valveTimerHandler(void){
    ++timeoutcounter;
}

void valvesInit(){
    
    // Release all pressure from machine during boot, leave valve open. Pressure functions will close it later
    openValve(PRESSURE_DECREASE_VALVE);

    //Multifunction pressurebox release mechanism
    openValve(MODE_VALVE_2);
    openValve(MODE_VALVE_4);
    openValve(MODE_VALVE_6);
        
    TMR4_SetInterruptHandler(valveTimerHandler);
}

void openValve(uint8_t valveNumber){
    IO_RA7_SetHigh();
    if(valveNumber <= VALVE_NUMBER_MAX){
        bool normallyOpen = (1<<(valveNumber-1) & valvePolarity);
        if(normallyOpen){
            setValveLow(valveNumber);
        } else {
            setValveHigh(valveNumber);
        }
        valvestates[valveNumber-1] = 1;
    }
}

void closeValve(uint8_t valveNumber){
    if(valveNumber <= VALVE_NUMBER_MAX){
        bool normallyOpen = (1<<(valveNumber-1) & valvePolarity);
        if(normallyOpen){
            setValveHigh(valveNumber);
        } else {
            setValveLow(valveNumber);
        }
        valvestates[valveNumber-1] = 0;
    }
}

static void setValveHigh(uint8_t valveNumber){
        switch(valveNumber){
        case 1:
            VALVE0_SetHigh(); // Open valve 1
            break;
        case 2:
            VALVE1_SetHigh(); // Open valve 2
            break;
        case 3:
            VALVE2_SetHigh(); // Open valve 3
            break;            
        case 4:
            VALVE3_SetHigh(); // Open valve 4
            break;
        case 5:
            VALVE4_SetHigh(); // Open valve 5
            break;
        case 6:
            VALVE5_SetHigh(); // Open valve 6
            break;
        case 7:
            VALVE6_SetHigh(); // Open valve 7
            break;
        case 8:
            VALVE7_SetHigh(); // Open valve 8
            break;
        default:
            break;
    }
}

static void setValveLow(uint8_t valveNumber){
    switch(valveNumber){
        case 1:
            VALVE0_SetLow(); // Open valve 1
            break;
        case 2:
            VALVE1_SetLow(); // Open valve 2
            break;
        case 3:
            VALVE2_SetLow(); // Open valve 3
            break;            
        case 4:
            VALVE3_SetLow(); // Open valve 4
            break;
        case 5:
            VALVE4_SetLow(); // Open valve 5
            break;
        case 6:
            VALVE5_SetLow(); // Open valve 6
            break;
        case 7:
            VALVE6_SetLow(); // Open valve 7
            break;
        case 8:
            VALVE7_SetLow(); // Open valve 8
            break;
        default:
            break;
    }
}

void setValvePolarity(uint8_t polarity){
    valvePolarity = polarity;
}

void enableValveWatchDog(uint8_t valveNumber){
    watchDogValve = valveNumber;
    timeoutcounter = 0;
    TMR4_WriteTimer(0);
    TMR4_Start();
}

void disableValveWatchDog(){
    TMR4_Stop();
    watchDogValve = 0;
    valveTimeout = false;
}

void resetValveWatchDog(){
    if(watchDogValve != 0){
        TMR4_WriteTimer(0);
        timeoutcounter = 0;
    }    
}

void valveWatchDogHandlder(){
    uint8_t timeout;
    if(watchDogValve != 0){
        timeout = timeoutcounter;
        if(timeout >= VALVE_TIMEOUT){
            TMR4_Stop();
            closeValve(watchDogValve);
            watchDogValve = 0;
            
        }
    }
}


void handleValveWatchDog(uint8_t valveNumber, bool watchDogEnabled){
    if(watchDogValve == valveNumber){
        disableValveWatchDog();
    }       
    if(watchDogEnabled) {
        enableValveWatchDog(valveNumber);
    }    
}

void completeValveInit(void){
    if(valveInitCompleted == false){
        valveInitCompleted = true;
        closeValve(MODE_VALVE_2);
        closeValve(MODE_VALVE_4);
        closeValve(MODE_VALVE_6);
    }
}