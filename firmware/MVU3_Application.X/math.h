
#ifndef MATH_H
#define	MATH_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include <stdint.h>


uint16_t abs_16(int16_t value);



#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* MATH_H */

