/* 
 * File:   mvu.h
 * Author: onkila
 *
 * Created on October 16, 2020, 10:49 AM
 */

#ifndef MVU_H
#define	MVU_H

#ifdef	__cplusplus
extern "C" {
#endif

void MVUBoot();


#ifdef	__cplusplus
}
#endif

#endif	/* MVU_H */

