/* 
 * File:   motorsAndValves.h
 * Author: Tapio Yliniemi
 *
 * Updated on November 23, 2022
 */


#ifndef MOTORS_H
#define	MOTORS_H

#include <stdint.h>
#include <stdbool.h>

#define DEFAULT_MOTOR_MAX_VALUE     0x7530

/* Motor speed defaults */
//#define MINIMUM_SPEED               0x7F    // 127. Specially linak motors move really slowly if duty cycle is below of this
#define RESET_SPEED                     0xC8    // 200
#define MAXIMUM_SPEED                   0xFF    // 255
// #define MAXIMUM_SPEED               0xC8    // 200
#define MOTOR_DETECT_DUTY_VALUE         0x3FF   // 1023
#define MOTOR_START_DUTY_VALUE          0x3FF   // 1023
#define MINIMUM_DUTY_VALUE              0x200   // 512
#define DETECT_DUTY_INCREASE_VALUE      50
// #define UP_SLOW_MOTION_POINT 19/20
// #define UP_SLOW_MOTION_POINT            100
// #define DOWN_SLOW_MOTION_POINT          100
#define TOP_BRAKING_POINT               5       // %
#define BOTTOM_BRAKING_POINT            1       // %
#define MINIMUM_TOP_BRAKING_POINT       20      // Motor rounds
#define MAXIMUM_BOTTOM_BRAKING_POINT    20      // Motor rounds
#define DUTY_DECREASE_VALUE             5
#define DUTY_INCREASE_VALUE             2
#define MAXIMUM_HEIGHT                  32000

#define GLOBAL_MOTOR_TIMEOUT            1000
#define LISTEN_REED_TICKS_TIME          500
#define MOTOR_DETECTION_TIME            1000
#define MOTOR_DETECTION_RUN_TIME        1200

#define MOTOR_RUN_DETECTION_ROUNDS      5

bool kickStart;

void motorOneGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts);
void motorTwoGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts);
void motorThreeGoToPosition(uint16_t value, uint8_t speed, bool enableInterrupts);
void motorOneMove(uint8_t direction, uint8_t speed, bool useLimits);
void motorTwoMove(uint8_t direction, uint8_t speed, bool useLimits);
void motorThreeMove(uint8_t direction, uint8_t speed, bool useLimits);
// void ReadSPIregisters(uint8_t controllerNumber, uint8_t command);
void updateSlaveData();
void resetAllMotors(uint8_t * offsets);
void motorsMoveDown();
bool detectMotors(uint8_t motorNumber);
void setMotorDutyValue(int16_t value);

typedef struct MotorStatusDef{
    int16_t currentHeight;
    uint16_t maximumHeight;
}MotorsStatus;

int16_t motorDutyValue;


#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* MOTORS_H */


