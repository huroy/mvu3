/**
  Generated Pin Manager File

  Company:
    Microchip Technology Inc.

  File Name:
    pin_manager.c

  Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F19185
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB             :  MPLAB X 5.45

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "pin_manager.h"
#include "stdbool.h"



#if (__XC8_VERSION <= 1410) 

typedef union {
    struct {
        unsigned T2INPPS                :6;
    };
    struct {
        unsigned T2INPPS0               :1;
        unsigned T2INPPS1               :1;
        unsigned T2INPPS2               :1;
        unsigned T2INPPS3               :1;
        unsigned T2INPPS4               :1;
        unsigned T2INPPS5               :1;
    };
} T2INPPSbits_t;
extern volatile T2INPPSbits_t T2INPPSbits @ (&T2AINPPS);

typedef union {
    struct {
        unsigned T4INPPS                :6;
    };
    struct {
        unsigned T4INPPS0               :1;
        unsigned T4INPPS1               :1;
        unsigned T4INPPS2               :1;
        unsigned T4INPPS3               :1;
        unsigned T4INPPS4               :1;
        unsigned T4INPPS5               :1;
    };
} T4INPPSbits_t;
extern volatile T4INPPSbits_t T4INPPSbits @ (&T4AINPPS);
#endif

void PIN_MANAGER_Initialize(void)
{
    /**
    LATx registers
    */
    LATE = 0x00;
    LATD = 0x00;
    LATA = 0x00;
    LATF = 0x00;
    LATB = 0x00;
    LATC = 0x00;

    /**
    TRISx registers
    */
    TRISE = 0x00;
    TRISF = 0xE0;
    TRISA = 0x0F;
    TRISB = 0x37;
    TRISC = 0x80;
    TRISD = 0xC0;

    /**
    ANSELx registers
    */
    // ANSELD = 0xFF;
    // ANSELC = 0x5C;
    // ANSELB = 0xC8;
    // ANSELE = 0x03;
    // ANSELF = 0x1F;
    // ANSELA = 0xDF;

    ANSELD = 0xC0;
    ANSELC = 0x00;
    ANSELB = 0x30;
    ANSELE = 0x00;
    ANSELF = 0x00;
    ANSELA = 0x0F;


    /**
    WPUx registers
    */
    WPUD = 0x00;
    WPUF = 0x00;
    WPUE = 0x00;
    WPUB = 0x00;
    WPUA = 0x00;
    WPUC = 0x00;

    /**
    ODx registers
    */
    ODCONE = 0x00;
    ODCONF = 0x00;
    ODCONA = 0x00;
    ODCONB = 0x00;
    ODCONC = 0x00;
    ODCOND = 0x00;

    /**
    SLRCONx registers
    */
    SLRCONA = 0xDF;
    SLRCONB = 0xFF;
    SLRCONC = 0xDF;
    SLRCOND = 0xFF;
    SLRCONE = 0x03;
    SLRCONF = 0xFF;

    /**
    INLVLx registers
    */
    INLVLA = 0xFF;
    INLVLB = 0xFF;
    INLVLC = 0xDF;
    INLVLD = 0xFF;
    INLVLE = 0x0B;
    INLVLF = 0xFF;
    

   
    
      
/*  These setting was moved to the bootloader  
 * 
 * NOTE: Do not use RX1 inverting for MCP200x LIN driver because it has not 
 * inverting signalling. Use inverting for TIOL111x driver only.
 * 
 *  
     bool state = (unsigned char)GIE;
    GIE = 0;
    
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x00; // unlock PPS

    RX1PPS = 0x2C;   //RF4->EUSART1:RX1    
    RC6PPS = 0x0D;   //RC6->EUSART1:TX1
    
    // CONFIGURABLE LOGIC CELL(CLC1)is used for EURART1 RX signal inverting 
     
    CLCIN0PPS = 0x17;   //RC7->CLC1:CLCIN0;
    RF4PPS = 0x01;   //RA7->CLC1:CLC1OUT;
    
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 0x01; // lock PPS
    

    // CLC1 input CLCIN0PPS is selected
     
    CLC1SEL0 = 0x00; 
    CLC1SEL1 = 0x00; 
    CLC1SEL2 = 0x00; 
    CLC1SEL3 = 0x00; 

    //  LCxG1D1T: Gate 0 Data 1 True (non-inverted) bit
    //    1 = CLCIN0 (true) is gated into CLCx Gate 0
        
    CLC1GLS0 = 0x02;
    CLC1GLS1 = 0x00;
    CLC1GLS2 = 0x00;
    CLC1GLS3 = 0x00;
    
    CLC1CON = 0x80; // CLC1 cells enabled with AND-OR operation
    
    CLC1POL =0x03;  // Config CLC1 from RX to invert EUSART1 RX input
    GIE = state;
    The end of section "these setting was moved to the bootloader"   */ 
    
}
  
void PIN_MANAGER_IOC(void)
{   
}

/**
 End of File
*/