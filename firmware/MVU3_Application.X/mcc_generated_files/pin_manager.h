/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F19185
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set analog input SENSEM1 aliases
#define SENSEM1_TRIS                 TRISAbits.TRISA0
#define SENSEM1_LAT                  LATAbits.LATA0
#define SENSEM1_PORT                 PORTAbits.RA0
#define SENSEM1_WPU                  WPUAbits.WPUA0
#define SENSEM1_OD                   ODCONAbits.ODCA0
#define SENSEM1_ANS                  ANSELAbits.ANSA0
#define SENSEM1_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define SENSEM1_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define SENSEM1_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define SENSEM1_GetValue()           PORTAbits.RA0
#define SENSEM1_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define SENSEM1_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define SENSEM1_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define SENSEM1_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define SENSEM1_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define SENSEM1_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define SENSEM1_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define SENSEM1_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set analog input SENSEM2 aliases
#define SENSEM2_TRIS                 TRISAbits.TRISA1
#define SENSEM2_LAT                  LATAbits.LATA1
#define SENSEM2_PORT                 PORTAbits.RA1
#define SENSEM2_WPU                  WPUAbits.WPUA1
#define SENSEM2_OD                   ODCONAbits.ODCA1
#define SENSEM2_ANS                  ANSELAbits.ANSA1
#define SENSEM2_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define SENSEM2_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define SENSEM2_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define SENSEM2_GetValue()           PORTAbits.RA1
#define SENSEM2_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define SENSEM2_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define SENSEM2_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define SENSEM2_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define SENSEM2_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define SENSEM2_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define SENSEM2_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define SENSEM2_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set analog input SENSEM3 aliases
#define SENSEM3_TRIS                 TRISAbits.TRISA2
#define SENSEM3_LAT                  LATAbits.LATA2
#define SENSEM3_PORT                 PORTAbits.RA2
#define SENSEM3_WPU                  WPUAbits.WPUA2
#define SENSEM3_OD                   ODCONAbits.ODCA2
#define SENSEM3_ANS                  ANSELAbits.ANSA2
#define SENSEM3_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define SENSEM3_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define SENSEM3_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define SENSEM3_GetValue()           PORTAbits.RA2
#define SENSEM3_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define SENSEM3_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define SENSEM3_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define SENSEM3_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define SENSEM3_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define SENSEM3_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define SENSEM3_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define SENSEM3_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set RA3 procedures
// Used for VREF
#define RA3_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define RA3_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define RA3_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define RA3_GetValue()              PORTAbits.RA3
#define RA3_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define RA3_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define RA3_SetPullup()             do { WPUAbits.WPUA3 = 1; } while(0)
#define RA3_ResetPullup()           do { WPUAbits.WPUA3 = 0; } while(0)
#define RA3_SetAnalogMode()         do { ANSELAbits.ANSA3 = 1; } while(0)
#define RA3_SetDigitalMode()        do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set LIN2_M3 aliases
#define LIN2_M3_TRIS                 TRISAbits.TRISA4
#define LIN2_M3_LAT                  LATAbits.LATA4
#define LIN2_M3_PORT                 PORTAbits.RA4
#define LIN2_M3_WPU                  WPUAbits.WPUA4
#define LIN2_M3_OD                   ODCONAbits.ODCA4
#define LIN2_M3_ANS                  ANSELAbits.ANSA4
#define LIN2_M3_SetOn()            do { LATAbits.LATA4 = 1; } while(0)
#define LIN2_M3_SetOff()             do { LATAbits.LATA4 = 0; } while(0)
#define LIN2_M3_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define LIN2_M3_GetValue()           PORTAbits.RA4
#define LIN2_M3_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define LIN2_M3_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define LIN2_M3_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define LIN2_M3_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define LIN2_M3_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define LIN2_M3_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define LIN2_M3_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define LIN2_M3_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set IO_RA5 aliases
#define IO_RA5_PORT                 PORTAbits.RA5
#define IO_RA5_WPU                  WPUAbits.WPUA5
#define IO_RA5_GetValue()           PORTAbits.RA5
#define IO_RA5_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define IO_RA5_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)

// get/set LIN2_M2 aliases
#define LIN2_M2_TRIS                 TRISAbits.TRISA6
#define LIN2_M2_LAT                  LATAbits.LATA6
#define LIN2_M2_PORT                 PORTAbits.RA6
#define LIN2_M2_WPU                  WPUAbits.WPUA6
#define LIN2_M2_OD                   ODCONAbits.ODCA6
#define LIN2_M2_ANS                  ANSELAbits.ANSA6
#define LIN2_M2_SetOn()            do { LATAbits.LATA6 = 1; } while(0)
#define LIN2_M2_SetOff()             do { LATAbits.LATA6 = 0; } while(0)
#define LIN2_M2_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define LIN2_M2_GetValue()           PORTAbits.RA6
#define LIN2_M2_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define LIN2_M2_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define LIN2_M2_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define LIN2_M2_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define LIN2_M2_SetPushPull()        do { ODCONAbits.ODCA6 = 0; } while(0)
#define LIN2_M2_SetOpenDrain()       do { ODCONAbits.ODCA6 = 1; } while(0)
#define LIN2_M2_SetAnalogMode()      do { ANSELAbits.ANSA6 = 1; } while(0)
#define LIN2_M2_SetDigitalMode()     do { ANSELAbits.ANSA6 = 0; } while(0)

// get/set IO_RA7 aliases
#define IO_RA7_TRIS                 TRISAbits.TRISA7
#define IO_RA7_LAT                  LATAbits.LATA7
#define IO_RA7_PORT                 PORTAbits.RA7
#define IO_RA7_WPU                  WPUAbits.WPUA7
#define IO_RA7_OD                   ODCONAbits.ODCA7
#define IO_RA7_ANS                  ANSELAbits.ANSA7
#define IO_RA7_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define IO_RA7_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define IO_RA7_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define IO_RA7_GetValue()           PORTAbits.RA7
#define IO_RA7_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define IO_RA7_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define IO_RA7_SetPullup()          do { WPUAbits.WPUA7 = 1; } while(0)
#define IO_RA7_ResetPullup()        do { WPUAbits.WPUA7 = 0; } while(0)
#define IO_RA7_SetPushPull()        do { ODCONAbits.ODCA7 = 0; } while(0)
#define IO_RA7_SetOpenDrain()       do { ODCONAbits.ODCA7 = 1; } while(0)
#define IO_RA7_SetAnalogMode()      do { ANSELAbits.ANSA7 = 1; } while(0)
#define IO_RA7_SetDigitalMode()     do { ANSELAbits.ANSA7 = 0; } while(0)

// get/set PLEX0 aliases
#define PLEX0_TRIS                 TRISBbits.TRISB0
#define PLEX0_LAT                  LATBbits.LATB0
#define PLEX0_PORT                 PORTBbits.RB0
#define PLEX0_WPU                  WPUBbits.WPUB0
#define PLEX0_OD                   ODCONBbits.ODCB0
#define PLEX0_ANS                  ANSELBbits.ANSB0
#define PLEX0_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define PLEX0_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define PLEX0_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define PLEX0_GetValue()           PORTBbits.RB0
#define PLEX0_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define PLEX0_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define PLEX0_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define PLEX0_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define PLEX0_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define PLEX0_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define PLEX0_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define PLEX0_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set PLEX1 aliases
#define PLEX1_TRIS                 TRISBbits.TRISB1
#define PLEX1_LAT                  LATBbits.LATB1
#define PLEX1_PORT                 PORTBbits.RB1
#define PLEX1_WPU                  WPUBbits.WPUB1
#define PLEX1_OD                   ODCONBbits.ODCB1
#define PLEX1_ANS                  ANSELBbits.ANSB1
#define PLEX1_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define PLEX1_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define PLEX1_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define PLEX1_GetValue()           PORTBbits.RB1
#define PLEX1_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define PLEX1_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define PLEX1_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define PLEX1_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define PLEX1_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define PLEX1_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define PLEX1_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define PLEX1_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set PLEX2 aliases
#define PLEX2_TRIS                 TRISBbits.TRISB2
#define PLEX2_LAT                  LATBbits.LATB2
#define PLEX2_PORT                 PORTBbits.RB2
#define PLEX2_WPU                  WPUBbits.WPUB2
#define PLEX2_OD                   ODCONBbits.ODCB2
#define PLEX2_ANS                  ANSELBbits.ANSB2
#define PLEX2_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define PLEX2_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define PLEX2_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define PLEX2_GetValue()           PORTBbits.RB2
#define PLEX2_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define PLEX2_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define PLEX2_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define PLEX2_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define PLEX2_SetPushPull()        do { ODCONBbits.ODCB2 = 0; } while(0)
#define PLEX2_SetOpenDrain()       do { ODCONBbits.ODCB2 = 1; } while(0)
#define PLEX2_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define PLEX2_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set IO_RB3 aliases
#define IO_RB3_TRIS                 TRISBbits.TRISB3
#define IO_RB3_LAT                  LATBbits.LATB3
#define IO_RB3_PORT                 PORTBbits.RB3
#define IO_RB3_WPU                  WPUBbits.WPUB3
#define IO_RB3_OD                   ODCONBbits.ODCB3
#define IO_RB3_ANS                  ANSELBbits.ANSB3
// #define IO_RB3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define LIN_TX_Enable()             do { LATBbits.LATB3 = 1; } while(0)
// #define IO_RB3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define LIN_TX_Disable()             do { LATBbits.LATB3 = 0; } while(0)
#define IO_RB3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define Get_LIN_TX_Status()             PORTBbits.RB3
#define IO_RB3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define IO_RB3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define IO_RB3_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define IO_RB3_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define IO_RB3_SetPushPull()        do { ODCONBbits.ODCB3 = 0; } while(0)
#define IO_RB3_SetOpenDrain()       do { ODCONBbits.ODCB3 = 1; } while(0)
#define IO_RB3_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define IO_RB3_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set IO_RB4 aliases
#define IO_RB4_TRIS                 TRISBbits.TRISB4
#define IO_RB4_LAT                  LATBbits.LATB4
#define IO_RB4_PORT                 PORTBbits.RB4
#define IO_RB4_WPU                  WPUBbits.WPUB4
#define IO_RB4_OD                   ODCONBbits.ODCB4
#define IO_RB4_ANS                  ANSELBbits.ANSB4
#define IO_RB4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define IO_RB4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define IO_RB4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define IO_RB4_GetValue()           PORTBbits.RB4
#define IO_RB4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define IO_RB4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define IO_RB4_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define IO_RB4_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define IO_RB4_SetPushPull()        do { ODCONBbits.ODCB4 = 0; } while(0)
#define IO_RB4_SetOpenDrain()       do { ODCONBbits.ODCB4 = 1; } while(0)
#define IO_RB4_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define IO_RB4_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set IO_RB5 aliases
#define IO_RB5_TRIS                 TRISBbits.TRISB5
#define IO_RB5_LAT                  LATBbits.LATB5
#define IO_RB5_PORT                 PORTBbits.RB5
#define IO_RB5_WPU                  WPUBbits.WPUB5
#define IO_RB5_OD                   ODCONBbits.ODCB5
#define IO_RB5_ANS                  ANSELBbits.ANSB5
#define IO_RB5_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define IO_RB5_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define IO_RB5_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define IO_RB5_GetValue()           PORTBbits.RB5
#define IO_RB5_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define IO_RB5_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define IO_RB5_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define IO_RB5_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define IO_RB5_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define IO_RB5_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define IO_RB5_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define IO_RB5_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set ERROR_LED aliases
#define ERROR_LED_TRIS                 TRISBbits.TRISB6
#define ERROR_LED_LAT                  LATBbits.LATB6
#define ERROR_LED_PORT                 PORTBbits.RB6
#define ERROR_LED_WPU                  WPUBbits.WPUB6
#define ERROR_LED_OD                   ODCONBbits.ODCB6
#define ERROR_LED_ANS                  ANSELBbits.ANSB6
#define ERROR_LED_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define ERROR_LED_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define ERROR_LED_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define ERROR_LED_GetValue()           PORTBbits.RB6
#define ERROR_LED_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define ERROR_LED_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define ERROR_LED_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define ERROR_LED_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)
#define ERROR_LED_SetPushPull()        do { ODCONBbits.ODCB6 = 0; } while(0)
#define ERROR_LED_SetOpenDrain()       do { ODCONBbits.ODCB6 = 1; } while(0)
#define ERROR_LED_SetAnalogMode()      do { ANSELBbits.ANSB6 = 1; } while(0)
#define ERROR_LED_SetDigitalMode()     do { ANSELBbits.ANSB6 = 0; } while(0)

// get/set OK_LED aliases
#define OK_LED_TRIS                 TRISBbits.TRISB7
#define OK_LED_LAT                  LATBbits.LATB7
#define OK_LED_PORT                 PORTBbits.RB7
#define OK_LED_WPU                  WPUBbits.WPUB7
#define OK_LED_OD                   ODCONBbits.ODCB7
#define OK_LED_ANS                  ANSELBbits.ANSB7
#define OK_LED_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define OK_LED_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define OK_LED_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define OK_LED_GetValue()           PORTBbits.RB7
#define OK_LED_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define OK_LED_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define OK_LED_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define OK_LED_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define OK_LED_SetPushPull()        do { ODCONBbits.ODCB7 = 0; } while(0)
#define OK_LED_SetOpenDrain()       do { ODCONBbits.ODCB7 = 1; } while(0)
#define OK_LED_SetAnalogMode()      do { ANSELBbits.ANSB7 = 1; } while(0)
#define OK_LED_SetDigitalMode()     do { ANSELBbits.ANSB7 = 0; } while(0)

// get/set HIN2_M2 aliases
#define HIN2_M2_TRIS                 TRISCbits.TRISC0
#define HIN2_M2_LAT                  LATCbits.LATC0
#define HIN2_M2_PORT                 PORTCbits.RC0
#define HIN2_M2_WPU                  WPUCbits.WPUC0
#define HIN2_M2_OD                   ODCONCbits.ODCC0
#define HIN2_M2_SetOn()            do { LATCbits.LATC0 = 1; } while(0)
#define HIN2_M2_SetOff()             do { LATCbits.LATC0 = 0; } while(0)
#define HIN2_M2_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define HIN2_M2_GetValue()           PORTCbits.RC0
#define HIN2_M2_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define HIN2_M2_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define HIN2_M2_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define HIN2_M2_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define HIN2_M2_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define HIN2_M2_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)

// get/set LIN1_M2 aliases
#define LIN1_M2_TRIS                 TRISCbits.TRISC1
#define LIN1_M2_LAT                  LATCbits.LATC1
#define LIN1_M2_PORT                 PORTCbits.RC1
#define LIN1_M2_WPU                  WPUCbits.WPUC1
#define LIN1_M2_OD                   ODCONCbits.ODCC1
#define LIN1_M2_SetOn()            do { LATCbits.LATC1 = 1; } while(0)
#define LIN1_M2_SetOff()             do { LATCbits.LATC1 = 0; } while(0)
#define LIN1_M2_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define LIN1_M2_GetValue()           PORTCbits.RC1
#define LIN1_M2_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define LIN1_M2_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define LIN1_M2_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define LIN1_M2_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define LIN1_M2_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define LIN1_M2_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)

// get/set HIN1_M1 aliases
#define HIN1_M1_TRIS                 TRISCbits.TRISC2
#define HIN1_M1_LAT                  LATCbits.LATC2
#define HIN1_M1_PORT                 PORTCbits.RC2
#define HIN1_M1_WPU                  WPUCbits.WPUC2
#define HIN1_M1_OD                   ODCONCbits.ODCC2
#define HIN1_M1_ANS                  ANSELCbits.ANSC2
#define HIN1_M1_SetOn()            do { LATCbits.LATC2 = 1; } while(0)
#define HIN1_M1_SetOff()             do { LATCbits.LATC2 = 0; } while(0)
#define HIN1_M1_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define HIN1_M1_GetValue()           PORTCbits.RC2
#define HIN1_M1_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define HIN1_M1_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define HIN1_M1_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define HIN1_M1_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define HIN1_M1_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define HIN1_M1_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define HIN1_M1_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define HIN1_M1_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set VALVE7 aliases
#define VALVE7_TRIS                 TRISCbits.TRISC3
#define VALVE7_LAT                  LATCbits.LATC3
#define VALVE7_PORT                 PORTCbits.RC3
#define VALVE7_WPU                  WPUCbits.WPUC3
#define VALVE7_OD                   ODCONCbits.ODCC3
#define VALVE7_ANS                  ANSELCbits.ANSC3
#define VALVE7_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define VALVE7_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define VALVE7_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define VALVE7_GetValue()           PORTCbits.RC3
#define VALVE7_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define VALVE7_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define VALVE7_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define VALVE7_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define VALVE7_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define VALVE7_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define VALVE7_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define VALVE7_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set VALVE2 aliases
#define VALVE2_TRIS                 TRISCbits.TRISC4
#define VALVE2_LAT                  LATCbits.LATC4
#define VALVE2_PORT                 PORTCbits.RC4
#define VALVE2_WPU                  WPUCbits.WPUC4
#define VALVE2_OD                   ODCONCbits.ODCC4
#define VALVE2_ANS                  ANSELCbits.ANSC4
#define VALVE2_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define VALVE2_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define VALVE2_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define VALVE2_GetValue()           PORTCbits.RC4
#define VALVE2_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define VALVE2_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define VALVE2_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define VALVE2_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define VALVE2_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define VALVE2_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define VALVE2_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define VALVE2_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetPullup()             do { WPUCbits.WPUC6 = 1; } while(0)
#define RC6_ResetPullup()           do { WPUCbits.WPUC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RX1_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RX1_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RX1_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RX1_GetValue()              PORTCbits.RC7
#define RX1_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RX1_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RX1_SetPullup()             do { WPUCbits.WPUC7 = 1; } while(0)
#define RX1_ResetPullup()           do { WPUCbits.WPUC7 = 0; } while(0)
#define RX1_SetAnalogMode()         do { ANSELCbits.ANSC7 = 1; } while(0)
#define RX1_SetDigitalMode()        do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set VALVE6 aliases
#define VALVE6_TRIS                 TRISDbits.TRISD0
#define VALVE6_LAT                  LATDbits.LATD0
#define VALVE6_PORT                 PORTDbits.RD0
#define VALVE6_WPU                  WPUDbits.WPUD0
#define VALVE6_OD                   ODCONDbits.ODCD0
#define VALVE6_ANS                  ANSELDbits.ANSD0
#define VALVE6_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define VALVE6_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define VALVE6_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define VALVE6_GetValue()           PORTDbits.RD0
#define VALVE6_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define VALVE6_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define VALVE6_SetPullup()          do { WPUDbits.WPUD0 = 1; } while(0)
#define VALVE6_ResetPullup()        do { WPUDbits.WPUD0 = 0; } while(0)
#define VALVE6_SetPushPull()        do { ODCONDbits.ODCD0 = 0; } while(0)
#define VALVE6_SetOpenDrain()       do { ODCONDbits.ODCD0 = 1; } while(0)
#define VALVE6_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define VALVE6_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set VALVE5 aliases
#define VALVE5_TRIS                 TRISDbits.TRISD1
#define VALVE5_LAT                  LATDbits.LATD1
#define VALVE5_PORT                 PORTDbits.RD1
#define VALVE5_WPU                  WPUDbits.WPUD1
#define VALVE5_OD                   ODCONDbits.ODCD1
#define VALVE5_ANS                  ANSELDbits.ANSD1
#define VALVE5_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define VALVE5_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define VALVE5_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define VALVE5_GetValue()           PORTDbits.RD1
#define VALVE5_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define VALVE5_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define VALVE5_SetPullup()          do { WPUDbits.WPUD1 = 1; } while(0)
#define VALVE5_ResetPullup()        do { WPUDbits.WPUD1 = 0; } while(0)
#define VALVE5_SetPushPull()        do { ODCONDbits.ODCD1 = 0; } while(0)
#define VALVE5_SetOpenDrain()       do { ODCONDbits.ODCD1 = 1; } while(0)
#define VALVE5_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define VALVE5_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set VALVE4 aliases
#define VALVE4_TRIS                 TRISDbits.TRISD2
#define VALVE4_LAT                  LATDbits.LATD2
#define VALVE4_PORT                 PORTDbits.RD2
#define VALVE4_WPU                  WPUDbits.WPUD2
#define VALVE4_OD                   ODCONDbits.ODCD2
#define VALVE4_ANS                  ANSELDbits.ANSD2
#define VALVE4_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define VALVE4_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define VALVE4_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define VALVE4_GetValue()           PORTDbits.RD2
#define VALVE4_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define VALVE4_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define VALVE4_SetPullup()          do { WPUDbits.WPUD2 = 1; } while(0)
#define VALVE4_ResetPullup()        do { WPUDbits.WPUD2 = 0; } while(0)
#define VALVE4_SetPushPull()        do { ODCONDbits.ODCD2 = 0; } while(0)
#define VALVE4_SetOpenDrain()       do { ODCONDbits.ODCD2 = 1; } while(0)
#define VALVE4_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define VALVE4_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set VALVE3 aliases
#define VALVE3_TRIS                 TRISDbits.TRISD3
#define VALVE3_LAT                  LATDbits.LATD3
#define VALVE3_PORT                 PORTDbits.RD3
#define VALVE3_WPU                  WPUDbits.WPUD3
#define VALVE3_OD                   ODCONDbits.ODCD3
#define VALVE3_ANS                  ANSELDbits.ANSD3
#define VALVE3_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define VALVE3_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define VALVE3_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define VALVE3_GetValue()           PORTDbits.RD3
#define VALVE3_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define VALVE3_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define VALVE3_SetPullup()          do { WPUDbits.WPUD3 = 1; } while(0)
#define VALVE3_ResetPullup()        do { WPUDbits.WPUD3 = 0; } while(0)
#define VALVE3_SetPushPull()        do { ODCONDbits.ODCD3 = 0; } while(0)
#define VALVE3_SetOpenDrain()       do { ODCONDbits.ODCD3 = 1; } while(0)
#define VALVE3_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define VALVE3_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set VALVE1 aliases
#define VALVE1_TRIS                 TRISDbits.TRISD4
#define VALVE1_LAT                  LATDbits.LATD4
#define VALVE1_PORT                 PORTDbits.RD4
#define VALVE1_WPU                  WPUDbits.WPUD4
#define VALVE1_OD                   ODCONDbits.ODCD4
#define VALVE1_ANS                  ANSELDbits.ANSD4
#define VALVE1_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define VALVE1_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define VALVE1_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define VALVE1_GetValue()           PORTDbits.RD4
#define VALVE1_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define VALVE1_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define VALVE1_SetPullup()          do { WPUDbits.WPUD4 = 1; } while(0)
#define VALVE1_ResetPullup()        do { WPUDbits.WPUD4 = 0; } while(0)
#define VALVE1_SetPushPull()        do { ODCONDbits.ODCD4 = 0; } while(0)
#define VALVE1_SetOpenDrain()       do { ODCONDbits.ODCD4 = 1; } while(0)
#define VALVE1_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define VALVE1_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set VALVE0 aliases
#define VALVE0_TRIS                 TRISDbits.TRISD5
#define VALVE0_LAT                  LATDbits.LATD5
#define VALVE0_PORT                 PORTDbits.RD5
#define VALVE0_WPU                  WPUDbits.WPUD5
#define VALVE0_OD                   ODCONDbits.ODCD5
#define VALVE0_ANS                  ANSELDbits.ANSD5
#define VALVE0_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define VALVE0_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define VALVE0_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define VALVE0_GetValue()           PORTDbits.RD5
#define VALVE0_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define VALVE0_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define VALVE0_SetPullup()          do { WPUDbits.WPUD5 = 1; } while(0)
#define VALVE0_ResetPullup()        do { WPUDbits.WPUD5 = 0; } while(0)
#define VALVE0_SetPushPull()        do { ODCONDbits.ODCD5 = 0; } while(0)
#define VALVE0_SetOpenDrain()       do { ODCONDbits.ODCD5 = 1; } while(0)
#define VALVE0_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define VALVE0_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set channel_AND6 aliases
#define channel_AND6_TRIS                 TRISDbits.TRISD6
#define channel_AND6_LAT                  LATDbits.LATD6
#define channel_AND6_PORT                 PORTDbits.RD6
#define channel_AND6_WPU                  WPUDbits.WPUD6
#define channel_AND6_OD                   ODCONDbits.ODCD6
#define channel_AND6_ANS                  ANSELDbits.ANSD6
#define channel_AND6_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define channel_AND6_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define channel_AND6_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define channel_AND6_GetValue()           PORTDbits.RD6
#define channel_AND6_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define channel_AND6_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define channel_AND6_SetPullup()          do { WPUDbits.WPUD6 = 1; } while(0)
#define channel_AND6_ResetPullup()        do { WPUDbits.WPUD6 = 0; } while(0)
#define channel_AND6_SetPushPull()        do { ODCONDbits.ODCD6 = 0; } while(0)
#define channel_AND6_SetOpenDrain()       do { ODCONDbits.ODCD6 = 1; } while(0)
#define channel_AND6_SetAnalogMode()      do { ANSELDbits.ANSD6 = 1; } while(0)
#define channel_AND6_SetDigitalMode()     do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set channel_AND7 aliases
#define channel_AND7_TRIS                 TRISDbits.TRISD7
#define channel_AND7_LAT                  LATDbits.LATD7
#define channel_AND7_PORT                 PORTDbits.RD7
#define channel_AND7_WPU                  WPUDbits.WPUD7
#define channel_AND7_OD                   ODCONDbits.ODCD7
#define channel_AND7_ANS                  ANSELDbits.ANSD7
#define channel_AND7_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define channel_AND7_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define channel_AND7_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define channel_AND7_GetValue()           PORTDbits.RD7
#define channel_AND7_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define channel_AND7_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define channel_AND7_SetPullup()          do { WPUDbits.WPUD7 = 1; } while(0)
#define channel_AND7_ResetPullup()        do { WPUDbits.WPUD7 = 0; } while(0)
#define channel_AND7_SetPushPull()        do { ODCONDbits.ODCD7 = 0; } while(0)
#define channel_AND7_SetOpenDrain()       do { ODCONDbits.ODCD7 = 1; } while(0)
#define channel_AND7_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define channel_AND7_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set HIN2_M3 aliases
#define HIN2_M3_TRIS                 TRISEbits.TRISE0
#define HIN2_M3_LAT                  LATEbits.LATE0
#define HIN2_M3_PORT                 PORTEbits.RE0
#define HIN2_M3_WPU                  WPUEbits.WPUE0
#define HIN2_M3_OD                   ODCONEbits.ODCE0
#define HIN2_M3_ANS                  ANSELEbits.ANSE0
#define HIN2_M3_SetOn()            do { LATEbits.LATE0 = 1; } while(0)
#define HIN2_M3_SetOff()             do { LATEbits.LATE0 = 0; } while(0)
#define HIN2_M3_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define HIN2_M3_GetValue()           PORTEbits.RE0
#define HIN2_M3_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define HIN2_M3_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define HIN2_M3_SetPullup()          do { WPUEbits.WPUE0 = 1; } while(0)
#define HIN2_M3_ResetPullup()        do { WPUEbits.WPUE0 = 0; } while(0)
#define HIN2_M3_SetPushPull()        do { ODCONEbits.ODCE0 = 0; } while(0)
#define HIN2_M3_SetOpenDrain()       do { ODCONEbits.ODCE0 = 1; } while(0)
#define HIN2_M3_SetAnalogMode()      do { ANSELEbits.ANSE0 = 1; } while(0)
#define HIN2_M3_SetDigitalMode()     do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set LIN1_M3 aliases
#define LIN1_M3_TRIS                 TRISEbits.TRISE1
#define LIN1_M3_LAT                  LATEbits.LATE1
#define LIN1_M3_PORT                 PORTEbits.RE1
#define LIN1_M3_WPU                  WPUEbits.WPUE1
#define LIN1_M3_OD                   ODCONEbits.ODCE1
#define LIN1_M3_ANS                  ANSELEbits.ANSE1
#define LIN1_M3_SetOn()            do { LATEbits.LATE1 = 1; } while(0)
#define LIN1_M3_SetOff()             do { LATEbits.LATE1 = 0; } while(0)
#define LIN1_M3_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define LIN1_M3_GetValue()           PORTEbits.RE1
#define LIN1_M3_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define LIN1_M3_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define LIN1_M3_SetPullup()          do { WPUEbits.WPUE1 = 1; } while(0)
#define LIN1_M3_ResetPullup()        do { WPUEbits.WPUE1 = 0; } while(0)
#define LIN1_M3_SetPushPull()        do { ODCONEbits.ODCE1 = 0; } while(0)
#define LIN1_M3_SetOpenDrain()       do { ODCONEbits.ODCE1 = 1; } while(0)
#define LIN1_M3_SetAnalogMode()      do { ANSELEbits.ANSE1 = 1; } while(0)
#define LIN1_M3_SetDigitalMode()     do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set HIN1_M3 aliases
#define HIN1_M3_TRIS                 TRISEbits.TRISE2
#define HIN1_M3_LAT                  LATEbits.LATE2
#define HIN1_M3_PORT                 PORTEbits.RE2
#define HIN1_M3_WPU                  WPUEbits.WPUE2
#define HIN1_M3_ANS                  ANSELEbits.ANSE2
#define HIN1_M3_SetOn()            do { LATEbits.LATE2 = 1; } while(0)
#define HIN1_M3_SetOff()             do { LATEbits.LATE2 = 0; } while(0)
#define HIN1_M3_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define HIN1_M3_GetValue()           PORTEbits.RE2
#define HIN1_M3_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define HIN1_M3_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define HIN1_M3_SetPullup()          do { WPUEbits.WPUE2 = 1; } while(0)
#define HIN1_M3_ResetPullup()        do { WPUEbits.WPUE2 = 0; } while(0)
#define HIN1_M3_SetAnalogMode()      do { ANSELEbits.ANSE2 = 1; } while(0)
#define HIN1_M3_SetDigitalMode()     do { ANSELEbits.ANSE2 = 0; } while(0)

// get/set HIN1_M2 aliases
#define HIN1_M2_TRIS                 TRISFbits.TRISF0
#define HIN1_M2_LAT                  LATFbits.LATF0
#define HIN1_M2_PORT                 PORTFbits.RF0
#define HIN1_M2_WPU                  WPUFbits.WPUF0
#define HIN1_M2_OD                   ODCONFbits.ODCF0
#define HIN1_M2_ANS                  ANSELFbits.ANSF0
#define HIN1_M2_SetOn()            do { LATFbits.LATF0 = 1; } while(0)
#define HIN1_M2_SetOff()             do { LATFbits.LATF0 = 0; } while(0)
#define HIN1_M2_Toggle()             do { LATFbits.LATF0 = ~LATFbits.LATF0; } while(0)
#define HIN1_M2_GetValue()           PORTFbits.RF0
#define HIN1_M2_SetDigitalInput()    do { TRISFbits.TRISF0 = 1; } while(0)
#define HIN1_M2_SetDigitalOutput()   do { TRISFbits.TRISF0 = 0; } while(0)
#define HIN1_M2_SetPullup()          do { WPUFbits.WPUF0 = 1; } while(0)
#define HIN1_M2_ResetPullup()        do { WPUFbits.WPUF0 = 0; } while(0)
#define HIN1_M2_SetPushPull()        do { ODCONFbits.ODCF0 = 0; } while(0)
#define HIN1_M2_SetOpenDrain()       do { ODCONFbits.ODCF0 = 1; } while(0)
#define HIN1_M2_SetAnalogMode()      do { ANSELFbits.ANSF0 = 1; } while(0)
#define HIN1_M2_SetDigitalMode()     do { ANSELFbits.ANSF0 = 0; } while(0)

// get/set LIN2_M1 aliases
#define LIN2_M1_TRIS                 TRISFbits.TRISF1
#define LIN2_M1_LAT                  LATFbits.LATF1
#define LIN2_M1_PORT                 PORTFbits.RF1
#define LIN2_M1_WPU                  WPUFbits.WPUF1
#define LIN2_M1_OD                   ODCONFbits.ODCF1
#define LIN2_M1_ANS                  ANSELFbits.ANSF1
#define LIN2_M1_SetOn()            do { LATFbits.LATF1 = 1; } while(0)
#define LIN2_M1_SetOff()             do { LATFbits.LATF1 = 0; } while(0)
#define LIN2_M1_Toggle()             do { LATFbits.LATF1 = ~LATFbits.LATF1; } while(0)
#define LIN2_M1_GetValue()           PORTFbits.RF1
#define LIN2_M1_SetDigitalInput()    do { TRISFbits.TRISF1 = 1; } while(0)
#define LIN2_M1_SetDigitalOutput()   do { TRISFbits.TRISF1 = 0; } while(0)
#define LIN2_M1_SetPullup()          do { WPUFbits.WPUF1 = 1; } while(0)
#define LIN2_M1_ResetPullup()        do { WPUFbits.WPUF1 = 0; } while(0)
#define LIN2_M1_SetPushPull()        do { ODCONFbits.ODCF1 = 0; } while(0)
#define LIN2_M1_SetOpenDrain()       do { ODCONFbits.ODCF1 = 1; } while(0)
#define LIN2_M1_SetAnalogMode()      do { ANSELFbits.ANSF1 = 1; } while(0)
#define LIN2_M1_SetDigitalMode()     do { ANSELFbits.ANSF1 = 0; } while(0)

// get/set HIN2_M1 aliases
#define HIN2_M1_TRIS                 TRISFbits.TRISF2
#define HIN2_M1_LAT                  LATFbits.LATF2
#define HIN2_M1_PORT                 PORTFbits.RF2
#define HIN2_M1_WPU                  WPUFbits.WPUF2
#define HIN2_M1_OD                   ODCONFbits.ODCF2
#define HIN2_M1_ANS                  ANSELFbits.ANSF2
#define HIN2_M1_SetOn()            do { LATFbits.LATF2 = 1; } while(0)
#define HIN2_M1_SetOff()             do { LATFbits.LATF2 = 0; } while(0)
#define HIN2_M1_Toggle()             do { LATFbits.LATF2 = ~LATFbits.LATF2; } while(0)
#define HIN2_M1_GetValue()           PORTFbits.RF2
#define HIN2_M1_SetDigitalInput()    do { TRISFbits.TRISF2 = 1; } while(0)
#define HIN2_M1_SetDigitalOutput()   do { TRISFbits.TRISF2 = 0; } while(0)
#define HIN2_M1_SetPullup()          do { WPUFbits.WPUF2 = 1; } while(0)
#define HIN2_M1_ResetPullup()        do { WPUFbits.WPUF2 = 0; } while(0)
#define HIN2_M1_SetPushPull()        do { ODCONFbits.ODCF2 = 0; } while(0)
#define HIN2_M1_SetOpenDrain()       do { ODCONFbits.ODCF2 = 1; } while(0)
#define HIN2_M1_SetAnalogMode()      do { ANSELFbits.ANSF2 = 1; } while(0)
#define HIN2_M1_SetDigitalMode()     do { ANSELFbits.ANSF2 = 0; } while(0)

// get/set LIN1_M1 aliases
#define LIN1_M1_TRIS                 TRISFbits.TRISF3
#define LIN1_M1_LAT                  LATFbits.LATF3
#define LIN1_M1_PORT                 PORTFbits.RF3
#define LIN1_M1_WPU                  WPUFbits.WPUF3
#define LIN1_M1_OD                   ODCONFbits.ODCF3
#define LIN1_M1_ANS                  ANSELFbits.ANSF3
#define LIN1_M1_SetOn()            do { LATFbits.LATF3 = 1; } while(0)
#define LIN1_M1_SetOff()             do { LATFbits.LATF3 = 0; } while(0)
#define LIN1_M1_Toggle()             do { LATFbits.LATF3 = ~LATFbits.LATF3; } while(0)
#define LIN1_M1_GetValue()           PORTFbits.RF3
#define LIN1_M1_SetDigitalInput()    do { TRISFbits.TRISF3 = 1; } while(0)
#define LIN1_M1_SetDigitalOutput()   do { TRISFbits.TRISF3 = 0; } while(0)
#define LIN1_M1_SetPullup()          do { WPUFbits.WPUF3 = 1; } while(0)
#define LIN1_M1_ResetPullup()        do { WPUFbits.WPUF3 = 0; } while(0)
#define LIN1_M1_SetPushPull()        do { ODCONFbits.ODCF3 = 0; } while(0)
#define LIN1_M1_SetOpenDrain()       do { ODCONFbits.ODCF3 = 1; } while(0)
#define LIN1_M1_SetAnalogMode()      do { ANSELFbits.ANSF3 = 1; } while(0)
#define LIN1_M1_SetDigitalMode()     do { ANSELFbits.ANSF3 = 0; } while(0)

// get/set IO_RF4 aliases
#define IO_RF4_TRIS                 TRISFbits.TRISF4
#define IO_RF4_LAT                  LATFbits.LATF4
#define IO_RF4_PORT                 PORTFbits.RF4
#define IO_RF4_WPU                  WPUFbits.WPUF4
#define IO_RF4_OD                   ODCONFbits.ODCF4
#define IO_RF4_ANS                  ANSELFbits.ANSF4
#define IO_RF4_SetHigh()            do { LATFbits.LATF4 = 1; } while(0)
#define IO_RF4_SetLow()             do { LATFbits.LATF4 = 0; } while(0)
#define IO_RF4_Toggle()             do { LATFbits.LATF4 = ~LATFbits.LATF4; } while(0)
#define IO_RF4_GetValue()           PORTFbits.RF4
#define IO_RF4_SetDigitalInput()    do { TRISFbits.TRISF4 = 1; } while(0)
#define IO_RF4_SetDigitalOutput()   do { TRISFbits.TRISF4 = 0; } while(0)
#define IO_RF4_SetPullup()          do { WPUFbits.WPUF4 = 1; } while(0)
#define IO_RF4_ResetPullup()        do { WPUFbits.WPUF4 = 0; } while(0)
#define IO_RF4_SetPushPull()        do { ODCONFbits.ODCF4 = 0; } while(0)
#define IO_RF4_SetOpenDrain()       do { ODCONFbits.ODCF4 = 1; } while(0)
#define IO_RF4_SetAnalogMode()      do { ANSELFbits.ANSF4 = 1; } while(0)
#define IO_RF4_SetDigitalMode()     do { ANSELFbits.ANSF4 = 0; } while(0)

// get/set IO_RF5 aliases
#define MSENS3_TRIS                 TRISFbits.TRISF5
#define MSENS3_LAT                  LATFbits.LATF5
#define MSENS3_PORT                 PORTFbits.RF5
#define MSENS3_WPU                  WPUFbits.WPUF5
#define MSENS3_OD                   ODCONFbits.ODCF5
#define MSENS3_ANS                  ANSELFbits.ANSF5
#define MSENS3_SetHigh()            do { LATFbits.LATF5 = 1; } while(0)
#define MSENS3_SetLow()             do { LATFbits.LATF5 = 0; } while(0)
#define MSENS3_Toggle()             do { LATFbits.LATF5 = ~LATFbits.LATF5; } while(0)
#define MSENS3_GetValue()           PORTFbits.RF5
#define MSENS3_SetDigitalInput()    do { TRISFbits.TRISF5 = 1; } while(0)
#define MSENS3_SetDigitalOutput()   do { TRISFbits.TRISF5 = 0; } while(0)
#define MSENS3_SetPullup()          do { WPUFbits.WPUF5 = 1; } while(0)
#define MSENS3_ResetPullup()        do { WPUFbits.WPUF5 = 0; } while(0)
#define MSENS3_SetPushPull()        do { ODCONFbits.ODCF5 = 0; } while(0)
#define MSENS3_SetOpenDrain()       do { ODCONFbits.ODCF5 = 1; } while(0)
#define MSENS3_SetAnalogMode()      do { ANSELFbits.ANSF5 = 1; } while(0)
#define MSENS3_SetDigitalMode()     do { ANSELFbits.ANSF5 = 0; } while(0)

// get/set IO_RF6 aliases
#define MSENS2_TRIS                 TRISFbits.TRISF6
#define MSENS2_LAT                  LATFbits.LATF6
#define MSENS2_PORT                 PORTFbits.RF6
#define MSENS2_WPU                  WPUFbits.WPUF6
#define MSENS2_OD                   ODCONFbits.ODCF6
#define MSENS2_ANS                  ANSELFbits.ANSF6
#define MSENS2_SetHigh()            do { LATFbits.LATF6 = 1; } while(0)
#define MSENS2_SetLow()             do { LATFbits.LATF6 = 0; } while(0)
#define MSENS2_Toggle()             do { LATFbits.LATF6 = ~LATFbits.LATF6; } while(0)
#define MSENS2_GetValue()           PORTFbits.RF6
#define MSENS2_SetDigitalInput()    do { TRISFbits.TRISF6 = 1; } while(0)
#define MSENS2_SetDigitalOutput()   do { TRISFbits.TRISF6 = 0; } while(0)
#define MSENS2_SetPullup()          do { WPUFbits.WPUF6 = 1; } while(0)
#define MSENS2_ResetPullup()        do { WPUFbits.WPUF6 = 0; } while(0)
#define MSENS2_SetPushPull()        do { ODCONFbits.ODCF6 = 0; } while(0)
#define MSENS2_SetOpenDrain()       do { ODCONFbits.ODCF6 = 1; } while(0)
#define MSENS2_SetAnalogMode()      do { ANSELFbits.ANSF6 = 1; } while(0)
#define MSENS2_SetDigitalMode()     do { ANSELFbits.ANSF6 = 0; } while(0)

// get/set IO_RF7 aliases
#define MSENS1_TRIS                 TRISFbits.TRISF7
#define MSENS1_LAT                  LATFbits.LATF7
#define MSENS1_PORT                 PORTFbits.RF7
#define MSENS1_WPU                  WPUFbits.WPUF7
#define MSENS1_OD                   ODCONFbits.ODCF7
#define MSENS1_ANS                  ANSELFbits.ANSF7
#define MSENS1_SetHigh()            do { LATFbits.LATF7 = 1; } while(0)
#define MSENS1_SetLow()             do { LATFbits.LATF7 = 0; } while(0)
#define MSENS1_Toggle()             do { LATFbits.LATF7 = ~LATFbits.LATF7; } while(0)
#define MSENS1_GetValue()           PORTFbits.RF7
#define MSENS1_SetDigitalInput()    do { TRISFbits.TRISF7 = 1; } while(0)
#define MSENS1_SetDigitalOutput()   do { TRISFbits.TRISF7 = 0; } while(0)
#define MSENS1_SetPullup()          do { WPUFbits.WPUF7 = 1; } while(0)
#define MSENS1_ResetPullup()        do { WPUFbits.WPUF7 = 0; } while(0)
#define MSENS1_SetPushPull()        do { ODCONFbits.ODCF7 = 0; } while(0)
#define MSENS1_SetOpenDrain()       do { ODCONFbits.ODCF7 = 1; } while(0)
#define MSENS1_SetAnalogMode()      do { ANSELFbits.ANSF7 = 1; } while(0)
#define MSENS1_SetDigitalMode()     do { ANSELFbits.ANSF7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/