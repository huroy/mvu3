/**
  Generated Interrupt Manager Source File

  @Company:
    Microchip Technology Inc.

  @File Name:
    interrupt_manager.c
 * 
 * * 
 * Updated by Tapio Yliniemi 22.11.2021

  @Summary:
    This is the Interrupt Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description:
    This header file provides implementations for global interrupt handling.
    For individual peripheral handlers please see the peripheral driver for
    all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F19185
        Driver Version    :  2.04
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above or later
        MPLAB 	          :  MPLAB X 5.45
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "interrupt_manager.h"
#include "mcc.h"
#include "LINDrivers/lin_app.h"
#include "../full_bridge.h"
#include "../motors.h"

void __interrupt() INTERRUPT_InterruptManager (void)
{
   // interrupt handler
    if(PIE0bits.TMR0IE == 1 && PIR0bits.TMR0IF == 1)
    {
        TMR0_ISR();
    }
    else if(INTCONbits.PEIE == 1)
    {
         if(PIE3bits.TX1IE == 1 && PIR3bits.TX1IF == 1)
        {
            // EUSART1_TxDefaultInterruptHandler();
            EUSART1_Transmit_ISR();
        } 
        else if(PIE3bits.RC1IE == 1 && PIR3bits.RC1IF == 1)
        {
            // EUSART1_RxDefaultInterruptHandler();
            EUSART1_Receive_ISR();
        } 
        else if(PIE4bits.TMR4IE == 1 && PIR4bits.TMR4IF == 1)
        {
            TMR4_ISR();
        }
        else if(PIE5bits.CLC4IE == 1 && PIR5bits.CLC4IF == 1)
        {
            CLC4_ISR();
        }         
        /* ADC interrupt handler added by Tapio Yliniemi 22.11.2021*/
        else if(PIE1bits.ADIE == 1 && PIR1bits.ADIF == 1)
        {
            uint32_t result;
            uint32_t currentLimit;
            // static bool lowOverCurrentDetected = false;
            // static uint16_t currentPeakCount = 0;
            
            result = ((adc_result_t)((ADRESH << 8) + ADRESL));
            result = CURRENT_RESOLUTION * result;    // Read ADC value in mikrovolts
            
            
            /* If very high current pulse occurs, the current PWM value is set to 0
             immediately. Output stage driving is restarted */
            if(result > BRIDGE_OVERCURRENT_HIGH_LIMIT){
    
                    setMotorDutyValue(0);
                    bridgeStatus = OVERCURRENT;
                    ERROR_LED_SetHigh();
                    //}
                }
            
            
            /* If too high current pulse occurs, the current PWM value is decreased
             * after specified pulse count. */
            if(kickStart)currentLimit = BRIDGE_OVERCURRENT_KICK_LIMIT;
            else currentLimit = BRIDGE_OVERCURRENT_LOW_LIMIT;
            
            if(result > currentLimit || overCCount){
                // overCCount++;
                setMotorDutyValue(motorDutyValue - DUTY_INCREASE_VALUE);
                // lowOverCurrentDetected = true;
                // motorDutyValue = motorDutyValue - OVERCURRENT_LIMIT_STEP;
                // PWM2_LoadDutyValue(motorDutyValue);
                
                if(++overCCount > OVERCURRENT_COUNT){
                    //motorDutyValue = 0;
                    //PWM2_LoadDutyValue(motorDutyValue);
                    setMotorDutyValue(motorDutyValue - OVERCURRENT_LIMIT_STEP);
                    //setMotorDutyValue(0);
                    overCCount = OVERCURRENT_COUNT;
                    ERROR_LED_SetHigh();
                    // return;
                    }
                }
           
            /* If current limit is detected, output stage duty cycle is decreased by specified steps */
            if(result > BRIDGE_CURRENT_LIMIT){
                if(!kickStart)setMotorDutyValue(motorDutyValue - CURRENT_LIMIT_STEP);
           }
           else{
                if(!kickStart) overCCount = 0;
                bridgeStatus = NORMAL;
                ERROR_LED_SetLow();
           }
       PIR1bits.ADIF = 0;// Clear the ADCC interrupt flag            
            
        } 
        else if(PIE0bits.IOCIE && IOCCFbits.IOCCF7 && IOCCNbits.IOCCN7){
            
            if(!RX1_GetValue() && !PIE3bits.TX1IE){     // RX1 inverted for MCP2004 
                operationCancelled = true;
            }
            // clear the interrupt flag
            IOCCFbits.IOCCF7 = 0;
        }
        
        else
        {
            //Unhandled Interrupt
        }
    }      
    else
    {
        //Unhandled Interrupt
    }
}
/**
 End of File
*/
