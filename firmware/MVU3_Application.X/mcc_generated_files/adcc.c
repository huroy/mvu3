/**
  ADCC Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    adcc.c
 * 
 * * * 
 * Updated by Tapio Yliniemi 22.11.2021
 * New file for current measurement of fullbridge output stage

  @Summary
    This is the generated driver implementation file for the ADCC driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This source file provides implementations for driver APIs for ADCC.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F19185
        Driver Version    :  2.1.5
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB             :  MPLAB X 5.45
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "adcc.h"
#include "mcc.h"
#include "../eeprom_addresses.h"
#include "LINDrivers/lin_app.h"
#include "../full_bridge.h"

/**
  Section: ADCC Module Variables
*/
void (*ADCC_ADI_InterruptHandler)(void);
uint8_t convertREVToDecimal(adc_result_t);
/**
  Section: ADCC Module APIs
*/

void ADCC_Initialize(void)
{

    ADCC_updateOffset();
    
    // set the ADCC to the options selected in the User Interface
    // ADLTH 0; 
    ADLTHL = 0x00;
    // ADLTH 0; 
    ADLTHH = 0x00;
    // ADUTH 0; 
    ADUTHL = 0x00;
    // ADUTH 0; 
    ADUTHH = 0x00;
    // ADSTPT 0; 
    ADSTPTL = 0x00;
    // ADSTPT 0; 
    ADSTPTH = 0x00;
    // ADACC 0; 
    ADACCU = 0x00;
    // ADRPT 0; 
    ADRPT = 0x00;
    // ADPCH ANA0; 
    ADPCH = 0x00;
    // ADACQ 50 = 1.5us; 
    ADACQL = 0x32;  
    ADACQH = 0x00;
    // ADCAP Additional uC 28pF; 
    ADCAP = 0x0F;
    // ADPRE 0; 
    ADPREL = 0x00;
    // ADPRE 0; 
    ADPREH = 0x00;
    // ADDSEN disabled; ADGPOL digital_low; ADIPEN disabled; ADPPOL Vss; 
    ADCON1 = 0x00;
    // ADCRS 0; ADMD Basic_mode; ADACLR disabled; ADPSIS RES; 
    ADCON2 = 0x00;
    // ADCALC First derivative of Single measurement; ADTMD disabled; ADSOI ADGO not cleared; 
    ADCON3 = 0x00;
    // ADMATH registers not updated; 
    ADSTAT = 0x00;
    // ADPREF external; 
    ADREF = 0x02;
    // ADACT disabled; 
    ADACT = 0x00;
    // ADCS FOSC/40; 
    //ADCLK = 0x13;
    // ADCS FOSC/32; 
    ADCLK = 0x0F;
    // ADGO stop; ADFM right; ADON enabled; ADCS FOSC/ADCLK; ADCONT disabled; 
    ADCON0 = 0x84;
    // Discharge sampling capacitor
    ADC_CHANNEL = channel_Vss;

    ADCC_SetADIInterruptHandler(ADCC_DefaultInterruptHandler);
    // Clear the ADC interrupt flag
    PIR1bits.ADIF = 0;
    // Disabling ADCC interrupt.
    PIE1bits.ADIE = 0;
    
}

void ADCC_StartConversion(adcc_channel_t channel)
{
    // select the A/D channel
    ADPCH = channel;      
  
    // Turn on the ADC module
    ADCON0bits.ADON = 1;

    // Start the conversion
    ADCON0bits.ADGO = 1;
}

bool ADCC_IsConversionDone(void)
{
    // Start the conversion
    return ((unsigned char)(!ADCON0bits.ADGO));
}

adc_result_t ADCC_GetConversionResult(void)
{
    // Return the result
    return ((adc_result_t)((ADRESH << 8) + ADRESL));
}

adc_result_t ADCC_GetSingleScaledConversion(adcc_channel_t channel)
{
    uint32_t adccTemp, adccResult;
  
    // Disable ADCC interrupt.
    // PIE1bits.ADIE = 0;
    
// Disable ADCC interrupt.
    bool tempAdie = PIE1bits.ADIE;
    PIE1bits.ADIE = 0;    

    //Disable the continuous mode.
    ADCON0bits.ADCONT = 0; 
     // select the A/D channel
    ADPCH = channel;  
    // Turn on the ADC module
    ADCON0bits.ADON = 1;
    // Start the conversion
    ADCON0bits.ADGO = 1;
    
    // Wait for the conversion to finish
    while (ADCON0bits.ADGO)
    {
    }
    
    // Read result
    adccResult = ((adc_result_t)((ADRESH << 8) + ADRESL));
    
    // Discharge sampling capacitor
    ADC_CHANNEL = channel_Vss;
    
    // Clear the ADCC interrupt flag 
    // PIR1bits.ADIF = 0;
    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = tempAdie;

   /*  VREF correction and decimal shifting to right
    *  for calculations
    *  Resolution correction from 12 bit  to 10 bit
    *  adccResult is divided as small steps to be sure that
    *  all calculations are inside 32 bits ((adccResult/1000)/4)
    */ 
    adccResult = adccResult*SCALE_FACTOR; 
    adccResult = adccResult/40;
    adccResult = adccResult/100;

    if(adccResult>1023) adccResult = 1023;
    // Conversion finished, return the result
    return (adc_result_t)adccResult+adcOffset;    
}
    
   
    
adc_result_t ADCC_GetSingleFullResolutionConversion(adcc_channel_t channel)
{
    // Disable ADCC interrupt.
    PIE1bits.ADIE = 0;
    
    // select the A/D channel
    ADPCH = channel;  

    // Turn on the ADC module
    ADCON0bits.ADON = 1;
	
    //Disable the continuous mode.
    ADCON0bits.ADCONT = 0;    

    // Start the conversion
    ADCON0bits.ADGO = 1;
    
    // Wait for the conversion to finish
    while (ADCON0bits.ADGO)
    {
    }
    
    // Clear the ADCC interrupt flag 
    PIR1bits.ADIF = 0;
    
    // Conversion finished, return the result
    return ((adc_result_t)((ADRESH << 8) + ADRESL));
}

void ADCC_StopConversion(void)
{
    //Reset the ADGO bit.
    ADCON0bits.ADGO = 0;
}

void ADCC_SetStopOnInterrupt(void)
{
    //Set the ADSOI bit.
    ADCON3bits.ADSOI = 1;
}

void ADCC_DischargeSampleCapacitor(void)
{
    //Set the ADC channel to AVss.
    ADPCH = 0x3B;   
}

void ADCC_LoadAcquisitionRegister(uint16_t acquisitionValue)
{
    //Load the ADACQH and ADACQL registers.
    ADACQH = (uint8_t) (acquisitionValue >> 8); 
    ADACQL = (uint8_t) acquisitionValue;  
}

void ADCC_SetPrechargeTime(uint16_t prechargeTime)
{
    //Load the ADPREH and ADPREL registers.
    ADPREH = (uint8_t) (prechargeTime >> 8);  
    ADPREL = (uint8_t) prechargeTime;
}

void ADCC_SetRepeatCount(uint8_t repeatCount)
{
    //Load the ADRPT register.
    ADRPT = repeatCount;   
}

uint8_t ADCC_GetCurrentCountofConversions(void)
{
    //Return the contents of ADCNT register
    return ADCNT;
}

void ADCC_ClearAccumulator(void)
{
    //Reset the ADCON2bits.ADACLR bit.
    ADCON2bits.ADACLR = 1;
}

uint24_t ADCC_GetAccumulatorValue(void)
{
    //Return the contents of ADACCU, ADACCH and ADACCL registers
    return (((uint24_t)ADACCU << 16)+((uint24_t)ADACCH << 8) + ADACCL);
}

bool ADCC_HasAccumulatorOverflowed(void)
{
    //Return the status of ADSTATbits.ADAOV
    return ADSTATbits.ADAOV;
}

uint16_t ADCC_GetFilterValue(void)
{
    //Return the contents of ADFLTRH and ADFLTRL registers
    return ((uint16_t)((ADFLTRH << 8) + ADFLTRL));
}

uint16_t ADCC_GetPreviousResult(void)
{
    //Return the contents of ADPREVH and ADPREVL registers
    return ((uint16_t)((ADPREVH << 8) + ADPREVL));
}

void ADCC_DefineSetPoint(uint16_t setPoint)
{
    //Sets the ADSTPTH and ADSTPTL registers
    ADSTPTH = (uint8_t) (setPoint >> 8);
    ADSTPTL = (uint8_t) setPoint;
}

void ADCC_SetUpperThreshold(uint16_t upperThreshold)
{
    //Sets the ADUTHH and ADUTHL registers
    ADUTHH = (uint8_t) (upperThreshold >> 8);
    ADUTHL = (uint8_t) (upperThreshold);
}

void ADCC_SetLowerThreshold(uint16_t lowerThreshold)
{
    //Sets the ADLTHH and ADLTHL registers
    ADLTHH = (uint8_t) (lowerThreshold >> 8);
    ADLTHL = (uint8_t) lowerThreshold;
}

uint16_t ADCC_GetErrorCalculation(void)
{
	//Return the contents of ADERRH and ADERRL registers
	return ((uint16_t)((ADERRH << 8) + ADERRL));
}

void ADCC_EnableDoubleSampling(void)
{
    //Sets the ADCON1bits.ADDSEN
    ADCON1bits.ADDSEN = 1;
}

void ADCC_EnableContinuousConversion(void)
{
    //Sets the ADCON0bits.ADCONT
    ADCON0bits.ADCONT = 1;
}

void ADCC_DisableContinuousConversion(void)
{
    //Resets the ADCON0bits.ADCONT
    ADCON0bits.ADCONT = 0;
}

bool ADCC_HasErrorCrossedUpperThreshold(void)
{
    //Returns the value of ADSTATbits.ADUTHR bit.
    return ADSTATbits.ADUTHR;
}

bool ADCC_HasErrorCrossedLowerThreshold(void)
{
    //Returns the value of ADSTATbits.ADLTHR bit.
    return ADSTATbits.ADLTHR;
}

uint8_t ADCC_GetConversionStageStatus(void)
{
    //Returns the contents of ADSTATbits.ADSTAT field.
    return ADSTATbits.ADSTAT;
}

int ADCC_getAdcOffset(void){
    if(adcOffset == 0){
        return 205;
    }
    return adcOffset;
}

void ADCC_updateOffset(){
    int tempOffset = 0;
    if(EEPROM_READ(ADDRESS_ADC_OFFSET_HIGH)==1)
    {
        tempOffset=EEPROM_READ(ADDRESS_ADC_OFFSET_LOW);
        GETADCOFFSET_Data[0]=205+adcOffset;
       
    }
    else if(EEPROM_READ(ADDRESS_ADC_OFFSET_HIGH)==0)
    {
        tempOffset=EEPROM_READ(ADDRESS_ADC_OFFSET_LOW)*-1;
        GETADCOFFSET_Data[0]=205+adcOffset;
        
    }
    if(tempOffset > ADCC_OFFSET_MAX || tempOffset < ADCC_OFFSET_MIN){
        tempOffset = 0;
    }
    
    adcOffset = tempOffset;
    GETADCOFFSET_Data[0]=205+adcOffset;   
}

void ADCC_ISR(void)
{
 //   uint32_t result;
    
//    if (ADCC_ADI_InterruptHandler) ADCC_ADI_InterruptHandler();
/*   
    result = ((adc_result_t)((ADRESH << 8) + ADRESL));
    result = CURRENT_RESOLUTION * result;    // Read ADC value in mikrovolts
    if(result > BRIDGE_OVERCURRENT_LIMIT){
        disableOutputs();
        currentLimitDutyCycle = 0;
        ERROR_LED_SetHigh();
        bridgeStatus = OVERCURRENT;
        return;
    }
    if(result > BRIDGE_CURRENT_LIMIT){
        if(bridgeStatus == CURRENT_LIMIT){
            currentLimitDutyCycle = currentLimitDutyCycle - CURRENT_LIMIT_STEP;    
        }
        else{
            currentLimitDutyCycle = currentDutyCycle;
            bridgeStatus = CURRENT_LIMIT;
        }
    }
    else{
        if(currentLimitDutyCycle < currentDutyCycle){
            currentLimitDutyCycle++;
        }
        else{
            bridgeStatus = NORMAL;
        }
        
    }
    PIR1bits.ADIF = 0;// Clear the ADCC interrupt flag
    */
}
    

void ADCC_SetADIInterruptHandler(void (* InterruptHandler)(void)){
    ADCC_ADI_InterruptHandler = InterruptHandler;
}

void ADCC_DefaultInterruptHandler(void){
    // add your ADCC interrupt custom code
    // or set custom function using ADCC_SetADIInterruptHandler() or ADCC_SetADTIInterruptHandler()

 }

uint8_t getHWRevision(void){
    uint32_t HWrevision,tmp;
    
    adc_result_t result = ADCC_GetSingleFullResolutionConversion(channel_HWREV0);
    HWrevision = convertREVToDecimal(result);
    result = ADCC_GetSingleFullResolutionConversion(channel_HWREV1);
    tmp = convertREVToDecimal(result);
    tmp = tmp*10;
    return (uint8_t)(HWrevision+tmp);
}

uint8_t convertREVToDecimal(adc_result_t value){
    uint8_t number=0;
    if ( 300 < value) number = 1;       // 400 mV
    if ( 700 < value) number = 2;       // 800 mV
    if ( 1100 < value) number = 3;      // 1200 mV
    if ( 1500 < value) number = 4;      // 1600 mV
    if ( 1900 < value) number = 5;      // 2000 mV
    if ( 2300 < value) number = 6;      // 2400 mV
    if ( 2700 < value) number = 7;      // 2800 mV
    if ( 3100 < value) number = 8;      // 3200 mV
    if ( 3500 < value) number = 9;      // 3600 mV
    return number;
    }
/**
 End of File
*/
