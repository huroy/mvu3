/**
  LIN Slave Application
	
  Company:
    Microchip Technology Inc.

  File Name:
    lin_app.h

  Summary:
    LIN Slave Application

  Description:
    This header file provides the interface between the user and 
    the LIN drivers.

 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/


#ifndef LIN_APP_H
#define	LIN_APP_H

#include "lin_slave.h"

typedef enum {
     MOTORCTRL=6,               //6: PID 0x06
    ADJUSTPRESSURE,             //7: PID 0x47
    MOTORPOSITION,              //8: PID 0x08
    SETPOSITION,                //9:
    PRESSURE,                   //10:
    VALVECONTROL,               //11: PID 0x8B, valve number x, function (1=open, 0=closed), checksum
    READPRESSURE,               //12: PID 0x4C 
    GETERROR,                   //13:
    SETHWSW,                    //14: PID 0x8E, 
    GETHWSW,                    //15: PID 0xCF,
    RESETMOTORS,                //16: PID 0x50
    MOTORMOVE,                  //17: PID 0x11, motor number, dir 1/0, speed 0x64 - 0xff
    READREGISTERS,              //18:
    DEBUG,                      //19: PID 0xD3
    GETVALVESTATES,             //20: PID 0x14
    SETADCOFFSET,               //21: PID 0x55, data 8 bit
    GETADCOFFSET,               //22:
    VALVECONTROLWITHTIMEOUT,    //23:
    WRITEREGISTER,              //24:
    GETVALVEPOLARITY,           //25:
    MOVEMOTORSWITHOUTLIMITS,    //26: PID 0x1A
    RESETMOTORS_WITH_MAX_VALUE, //27: PID 5B, motor1 low offset, motor2 low offset, motor3 low offset,  motor1 high offset,  motor2 high offset,  motor3 high offset,      
    GETID=50,                   //50: PID 0x32
    RESETTOBOOTLOADER=51        //51:
}lin_cmd_t;

uint8_t MOTORCTRL_Data[4];
uint8_t ADJUSTPRESSURE_Data[5];
uint8_t MOTORPOSITION_Data[6];
uint8_t SETPOSITION_Data[6];
uint8_t PRESSURE_Data[2];
uint8_t VALVECONTROL_Data[2];
uint8_t READPRESSURE_Data[4];
uint8_t GETERROR_Data[2];
uint8_t SETHWSW_Data[4];
uint8_t GETSERIAL_Data[4];
uint8_t GETHWSW_Data[4];
uint8_t RESETMOTORS_Data[3];
uint8_t MOTORMOVE_Data[3];
uint8_t DEBUG_Data[6];
uint8_t RESETTOBOOTLOADER_Data[4];
uint8_t GETVALVESTATES_Data[8];
uint8_t SETADCOFFSET_Data[1];
uint8_t GETADCOFFSET_Data[1];
uint8_t VALVECONTROLTWITHTIMEOUT_Data[4];
uint8_t WRITEREGISTER_Data[3];
uint8_t GETVALVEPOLARITY_Data[1];
uint8_t MOTORMOVEWITHOUTLIMITS_Data[3];
uint8_t RESETMOTORS_WITH_MAX_VALUE_Data[6];
uint8_t READREGISTERS_Data[3]={0xFF,0xFF,0xFF};
uint8_t DeviceID[4]={0xff,0xff,0xff,0xff};

bool adjustInterrupted=false;
bool adjustFailed=false;
const lin_rx_cmd_t scheduleTable[] = {
     {MOTORCTRL, RECEIVE, 4, MOTORCTRL_Data},
    {ADJUSTPRESSURE, RECEIVE, 5, ADJUSTPRESSURE_Data},
    {MOTORPOSITION, TRANSMIT, 6, MOTORPOSITION_Data},
    {SETPOSITION, RECEIVE, 6, SETPOSITION_Data},
    {PRESSURE, RECEIVE, 2, PRESSURE_Data},
    {VALVECONTROL, RECEIVE, 2, VALVECONTROL_Data},
    {GETERROR, TRANSMIT, 2, GETERROR_Data},
    {SETHWSW, RECEIVE, 4, SETHWSW_Data},
    {READPRESSURE, TRANSMIT, 4, READPRESSURE_Data},
    {GETHWSW, TRANSMIT, 4, GETHWSW_Data},
    {RESETMOTORS, RECEIVE, 3, RESETMOTORS_Data},
    {MOTORMOVE, RECEIVE, 3, MOTORMOVE_Data},
    {DEBUG, TRANSMIT, 6, DEBUG_Data},
    {RESETTOBOOTLOADER, RECEIVE, 4, RESETTOBOOTLOADER_Data},
    {READREGISTERS, TRANSMIT, 3, READREGISTERS_Data},
    {GETVALVESTATES, TRANSMIT, 8, GETVALVESTATES_Data},
    {SETADCOFFSET, RECEIVE, 1, SETADCOFFSET_Data},
    {GETADCOFFSET, TRANSMIT, 1, GETADCOFFSET_Data},
    {VALVECONTROLWITHTIMEOUT, RECEIVE, 4, VALVECONTROLTWITHTIMEOUT_Data},
    {MOVEMOTORSWITHOUTLIMITS, RECEIVE, 3, MOTORMOVEWITHOUTLIMITS_Data},
    {RESETMOTORS_WITH_MAX_VALUE, RECEIVE, 6, RESETMOTORS_WITH_MAX_VALUE_Data},
    {WRITEREGISTER, RECEIVE, 3, WRITEREGISTER_Data},
    {GETVALVEPOLARITY, TRANSMIT, 1, GETVALVEPOLARITY_Data},  
};
#define TABLE_SIZE  (sizeof(scheduleTable)/sizeof(lin_rx_cmd_t))
#define LIN_CANCEL  (!RX1_GetValue() || operationCancelled)


static volatile unsigned bool operationCancelled = false;
static volatile unsigned bool cancelWaitOnGoing = false;

void LIN_Slave_Initialize(void);
void processLIN(void);
void runAdjustDelay(uint16_t time);

void adjustWithSafeLoad(uint16_t safeLoadValue, uint16_t targetPressure, uint8_t controlType);
void adjustWithoutSafeLoad(uint16_t targetPressure, uint8_t controlType) ;

// bool LIN_CANCEL();

void pauseCancelWait();
void startCancelWait();
void stopCancelWait();
void continueCancelWait();

void IOCC7_ISR();

#endif	/* LIN_APP_H */

