/**
  LIN Slave Application
	
  Company:
    Microchip Technology Inc.

  File Name:
    lin_app.c
 * 
 * * 
 * Updated by Tapio Yliniemi 22.11.2021
 * -    bool LIN_CANCEL()polarity changed
 *      //  return (RC7_GetValue() == 0 || operationCancelled);
            return (RC7_GetValue() == 1 || operationCancelled);
 * 
    -   //  if(RC7_GetValue() == 0 && (0 == PIE3bits.TX1IE)){ 
            if(RC7_GetValue() == 1 && (0 == PIE3bits.TX1IE)){ 

  Summary:
    LIN Slave Application

  Description:
    This source file provides the interface between the user and 
    the LIN drivers.

 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
 */
#include "../mcc.h"
#include "lin_app.h"
#include "../../motors.h"
#include "../../valves.h"
#include "../pin_manager.h"
#include "../../pressureadjust.h"
#include "../../time.h"
#include "../../eeprom_addresses.h"
#include "../../math.h"

#define PRESSURE_VALUE_MIN_LIMIT 150

const uint8_t SW_VERSION = 4;

bool valveInitCompleted = false;
bool normal_TX_State;

// static volatile unsigned bool cancelWaitOnGoing = false;

void completeValveInit(void);

#define LOCK_EEPROM 0x01

void LIN_Slave_Initialize(void) {

    LIN_init(TABLE_SIZE, scheduleTable, processLIN);
    
//    uint8_t hw_revision = getHWRevision();
//    uint8_t tmp_hw = EEPROM_READ(ADDRESS_HW_REVISION);
    
//    if (hw_revision != EEPROM_READ(ADDRESS_HW_REVISION)){
//        EEPROM_WRITE(ADDRESS_HW_REVISION, hw_revision);
//    }
    
    //Initialize GETHWSW response data.
    GETHWSW_Data[0] = SW_VERSION;
    GETHWSW_Data[1] = EEPROM_READ(ADDRESS_HW_REVISION);
//    GETHWSW_Data[1] = getHWRevision();
    GETHWSW_Data[2] = EEPROM_READ(ADDRESS_SERIAL_HIGH);
    GETHWSW_Data[3] = EEPROM_READ(ADDRESS_SERIAL_LOW);
    
    /* Lin cancel interrupt initialize*/
    IOCCPbits.IOCCP7 = 0;
    IOCCNbits.IOCCN7 = 0;
    IOCCFbits.IOCCF7 = 0;
    PIE0bits.IOCIE = 0;
    
    
    
    
    
}

void processLIN(void) {

    uint8_t tempRxData[8];
    uint8_t cmd;
    uint16_t targetPressure;
    uint16_t safeLoad;
    uint16_t reedAmount = 0;

    cmd = LIN_getPacket(tempRxData);
    
    resetValveWatchDog();
    
    if (cmd == MOTORCTRL) {
        reedAmount = ((tempRxData[1] << 8) | tempRxData[2]);
        startCancelWait();
        if (tempRxData[0] == 1) {
             motorOneGoToPosition(reedAmount, tempRxData[3], true); // Command motor 1 to position
        }
        if (tempRxData[0] == 2) {
             motorTwoGoToPosition(reedAmount, tempRxData[3], true); // Command motor 2 to position
        }
        if (tempRxData[0] == 3) {
             motorThreeGoToPosition(reedAmount, tempRxData[3], true); // Command motor 3 to position
        }
        stopCancelWait();
        
    } else if (cmd == ADJUSTPRESSURE) {

        startCancelWait();
        if (tempRxData[0] == 1 || tempRxData[0] == 2 || tempRxData[0] == 3) {

            completeValveInit();
            closeValve(PRESSURE_DECREASE_VALVE);
            __delay_ms(100);
            targetPressure = ((tempRxData[3] << 8) | tempRxData[4]);
            safeLoad = ((tempRxData[1] << 8) | tempRxData[2]);
            adjustWithSafeLoad(safeLoad, targetPressure, tempRxData[0]);

        }
        stopCancelWait();
        
    } else if (cmd == MOTORMOVE) {

        startCancelWait();
        if (tempRxData[0] == 1) {
             motorOneMove(tempRxData[1], tempRxData[2], true); // Run motor 1 
        } else if (tempRxData[0] == 2) {
             motorTwoMove(tempRxData[1], tempRxData[2], true); // Run motor 2
        } else if (tempRxData[0] == 3) {
             motorThreeMove(tempRxData[1], tempRxData[2], true); // Run motor 3
        }
        stopCancelWait();
        
    } else if(cmd == MOVEMOTORSWITHOUTLIMITS ) {
        startCancelWait();
        if (tempRxData[0] == 1) {
             motorOneMove(tempRxData[1], tempRxData[2], false); // Run motor 1 
        } else if (tempRxData[0] == 2) {
             motorTwoMove(tempRxData[1], tempRxData[2], false); // Run motor 2
        } else if (tempRxData[0] == 3) {
             motorThreeMove(tempRxData[1], tempRxData[2], false); // Run motor 3
        }
        stopCancelWait();
        
    } else if (cmd == READREGISTERS) {
        // ReadSPIregisters(1, 0x00); // NOT USED ATM
    } else if (cmd == SETPOSITION) {
        for (int i = 1; i < 7; i++) {
            EEPROM_WRITE(i, tempRxData[i - 1]); // NOT USED ATM
        }
        
    } else if (cmd == GETERROR) {
        // MVUBoot(); // NOT USED ATM
    } else if (cmd == VALVECONTROL) {   
        uint8_t valveNumber = tempRxData[0];
        bool open = (bool) tempRxData[1] & 0x01;
        bool watchDog = (bool) ((tempRxData[1] >> 1) & 0x01);
        
        completeValveInit();
        
        if(open){
            openValve(valveNumber);
        } else {
            closeValve(valveNumber);
        } 
        for (int i = 0; i < 8; i++) {
            GETVALVESTATES_Data[i] = valvestates[i];
        }
        handleValveWatchDog(valveNumber, watchDog);

    } else if (cmd == RESETMOTORS) // Factory setup motors
    {
        updateSlaveData();
        uint8_t zero_offsets[6] = {0,0,0,0,0,0}; 
        resetAllMotors(zero_offsets);

    } else if (cmd == RESETMOTORS_WITH_MAX_VALUE) // Factory setup motors
    {
        updateSlaveData();
        uint8_t offsets[6] = {0,0,0,0,0,0}; 
        offsets[0] = tempRxData[0];
        offsets[1] = tempRxData[1];
        offsets[2] = tempRxData[2];
        offsets[3] = tempRxData[3];
        offsets[4] = tempRxData[4];
        offsets[5] = tempRxData[5];
        
        resetAllMotors(offsets);

    }
    else if (cmd == RESETTOBOOTLOADER) { //Reset if ID is correct. 

        uint8_t SwRev = SW_VERSION;
        uint8_t HwRev = EEPROM_READ(2);
        uint8_t serialH = EEPROM_READ(3);
        uint8_t serialL = EEPROM_READ(4);



        if (tempRxData[0] == SwRev && tempRxData[1] == HwRev && tempRxData[2] == serialH && tempRxData[3] == serialL) // Write identifiers to card
        {
            EEPROM_WRITE(5, 0x01);
            RESET();
            __delay_ms(5);
        }

    }  else if (cmd == SETHWSW) {

        uint8_t SwRev = tempRxData[0];
        uint8_t HwRev = getHWRevision();
                    
        uint8_t serialH = tempRxData[2];
        uint8_t serialL = tempRxData[3];
        EEPROM_WRITE(ADDRESS_SW_REVISION, SW_VERSION);

        if (EEPROM_READ(ADDRESS_LOCK_SERIAL_SETTINGS) != 0) {
            EEPROM_WRITE(ADDRESS_LOCK_SERIAL_SETTINGS, 0x00); // LOCK SERIAL SETTING
            EEPROM_WRITE(2, HwRev);
            EEPROM_WRITE(ADDRESS_SERIAL_HIGH, serialH);
            EEPROM_WRITE(ADDRESS_SERIAL_LOW, serialL);
            
            //Update also response
            GETHWSW_Data[0] = SW_VERSION;
//            GETHWSW_Data[1] = getHWRevision();
            GETHWSW_Data[1] = HwRev;
            GETHWSW_Data[2] = serialH;
            GETHWSW_Data[3] = serialL;
        }
    } else if (cmd == READPRESSURE) {
        uint16_t pressureSensorOne = ADCC_GetSingleScaledConversion(channel_VSENS0);
        uint8_t low = pressureSensorOne & 0xff;
        uint8_t high = (pressureSensorOne >> 8);
        READPRESSURE_Data[0] = high;
        READPRESSURE_Data[1] = low;

        __delay_us(10);

        uint16_t pressureSensorTwo = ADCC_GetSingleScaledConversion(channel_VSENS1);                
        uint8_t pressureTwolow = pressureSensorTwo & 0xff;
        uint8_t pressureTwohigh = (pressureSensorTwo >> 8);
        READPRESSURE_Data[2] = pressureTwohigh;
        READPRESSURE_Data[3] = pressureTwolow;

        __delay_us(10);

    }
    else if(cmd==SETADCOFFSET)
    {
        if(tempRxData[0] == 0x00){
            int oldOffset = adcOffset;        
            if(EEPROM_READ(ADDRESS_LOCK_WRITING) != LOCK_EEPROM) {

                adcOffset = 0;
                uint16_t zeroPointPressure = ADCC_GetSingleScaledConversion(channel_VSENS0);
                //If difference is too high, do not accept the value
                if((zeroPointPressure > (512-ADCC_OFFSET_MAX)) && (zeroPointPressure < (512+ADCC_OFFSET_MAX))){
                    EEPROM_WRITE(ADDRESS_LOCK_WRITING, LOCK_EEPROM);
                    int offset=0;
                    offset=512-zeroPointPressure;

                    if(offset>=0)
                    {
                        EEPROM_WRITE(ADDRESS_ADC_OFFSET_HIGH, 1);
                        EEPROM_WRITE(ADDRESS_ADC_OFFSET_LOW,  offset & 0xff);
                        adcOffset=(offset&0xff);
                    }
                    else if(offset<0)
                    {
                        uint8_t writtenOffset = (uint8_t) abs_16(offset);
                        EEPROM_WRITE(ADDRESS_ADC_OFFSET_HIGH, 0);
                        EEPROM_WRITE(ADDRESS_ADC_OFFSET_LOW, writtenOffset);
                        adcOffset=offset;
                    }
                    ADCC_updateOffset();
                } else {
                    adcOffset = oldOffset;
                }
            }           
        }        
    } else if(cmd==VALVECONTROLWITHTIMEOUT)
    {
        uint8_t valveNumber = tempRxData[0];
        bool open = (bool) tempRxData[1];
        uint16_t timeoutMs = tempRxData[3];
        timeoutMs = timeoutMs << 8;
        timeoutMs = timeoutMs | tempRxData[2];
        completeValveInit();
        if(open){
            openValve(valveNumber);
            delay_ms(timeoutMs);
            closeValve(valveNumber);
        } else {
            closeValve(valveNumber);
            delay_ms(timeoutMs);
            openValve(valveNumber);
        }
        for (int i = 0; i < 8; i++) {
            GETVALVESTATES_Data[i] = valvestates[i];
        }
    } else if(cmd==WRITEREGISTER){
        //Command has 16 bit address, use only lower one
        uint8_t address = tempRxData[1];
        uint8_t data = tempRxData[2];
        
        if(address == ADDRESS_LOCK_WRITING){
            if(data == 0x00){
                EEPROM_WRITE(address, data);
            }
        }
        //Allow writing only when eeprom is not locked
        else if(EEPROM_READ(ADDRESS_LOCK_WRITING) != LOCK_EEPROM){
            switch (address){
                case ADDRESS_VALVE_POLARITY:              
                    EEPROM_WRITE(address, data);
                    GETVALVEPOLARITY_Data[0] = data;
                    setValvePolarity(data);   
                    break;
                case ADDRESS_ADC_OFFSET_HIGH:
                    if(data == 0x00 || data == 0x01){
                        EEPROM_WRITE(address, data);
                    }
                    break;
                case ADDRESS_ADC_OFFSET_LOW:
                    if(data < ADCC_OFFSET_MAX){
                        EEPROM_WRITE(address, data);
                    }
                    ADCC_updateOffset();  
                    break;
                default:
                    break;
            }
            //Disable writing always after write command
            EEPROM_WRITE(ADDRESS_LOCK_WRITING, LOCK_EEPROM);
        }
    }
}


void runAdjustDelay(uint16_t time) {
    for (int i = 0; i < time * 50; i++) {
        __delay_us(20);

        if (RX1_GetValue() == 1) {  // If new Rx signalling detected
            adjustInterrupted = true;
            break;
        }
    }
}

void enableDisableRX(bool select) {
    RC1STAbits.CREN = select;
    RC1IE = select;
}

void adjustWithSafeLoad(uint16_t safeLoadValue, uint16_t targetPressure, uint8_t controlType){
    int recentSystemPressure = 0;
    int pressureDifference = 0;
    uint16_t adjustTime = 0;
    uint16_t firstTarget = 0;
    uint16_t adjustStartPoint = 0;
    adjustFailed=false;

    recentSystemPressure = ADCC_GetSingleScaledConversion(channel_VSENS0);
    //Pressure should not be adjusted if sensor value is invalid
    //This can happen for example when sensor is not connected
    if(recentSystemPressure > PRESSURE_VALUE_MIN_LIMIT){


        if (safeLoadValue > 0)
            firstTarget = safeLoadValue;
        else
            firstTarget = targetPressure;


        if (recentSystemPressure >= targetPressure)
            pressureDifference = recentSystemPressure - targetPressure;
        else
            pressureDifference = targetPressure - recentSystemPressure;

        adjustTime = pressureDifference;
        finalPressureTarget = targetPressure;

        if (targetPressure >= 210) {


            if (safeLoadValue > 0) {
                pressureControl(firstTarget, controlType, 1);
                __delay_ms(2000);
            }

            if (pressureControl(targetPressure, controlType, 2)) {
                runAdjustDelay(adjustTime * 6);

                if (pressureControl(targetPressure, controlType, 3) && !adjustInterrupted) {
                    runAdjustDelay(adjustTime * 4);
                    pressureControl(targetPressure, controlType, 4);
                } 
            } 
        } else
            pressureControl(0, controlType, 1);

    } else {
        adjustFailed = true;
        //small delay ensures that result is received
        __delay_ms(200);
        }
    
    adjustInterrupted = false;
    if(adjustFailed) EUSART1_Write(0xfe);   //
    EUSART1_Write(0xff);                    //
}


/*
void adjustWithoutSafeLoad(uint16_t targetPressure, uint8_t controlType) {
    int recentSystemPressure = 0;
    int pressureDifference = 0;
    uint16_t adjustTime = 0;
    uint16_t firstTarget = 0;

    recentSystemPressure = ADCC_GetSingleScaledConversion(channel_VSENS0);

    
    if(recentSystemPressure > PRESSURE_VALUE_MIN_LIMIT){
        
        firstTarget = targetPressure;


        if (recentSystemPressure >= targetPressure)
            pressureDifference = recentSystemPressure - targetPressure;
        else
            pressureDifference = targetPressure - recentSystemPressure;

        adjustTime = pressureDifference;


        if (targetPressure >= 210) {

            //pressureControl(firstTarget);
            // pressureControl(targetPressure);


            if (pressureControl(firstTarget, controlType, 1)) {
                runAdjustDelay(adjustTime * 6);
                if (pressureControl(targetPressure, controlType, 2) && !adjustInterrupted) {
                    runAdjustDelay(adjustTime * 4);
                    if (pressureControl(targetPressure, controlType, 3)&& !adjustInterrupted) {
                    }
                }
            }
            *//*
            if (pressureControl(firstTarget)) {
                runAdjustDelay(adjustTime * 11);
                if (pressureControl(targetPressure) && !adjustInterrupted) {
                    runAdjustDelay(adjustTime * 8);
                    if (pressureControl(targetPressure)&& !adjustInterrupted) {
                        runAdjustDelay(adjustTime * 8);
                        if (!adjustInterrupted)
                            pressureControl(targetPressure);
                    }
                }
            }*/
/*        } else
            pressureControl(0, controlType, 1);

    }


    adjustInterrupted = false;
    for (int i = 0; i < 32; i++){ ??
        //pauseCancelWait();
        EUSART1_Write(0xff);
        //continueCancelWait();
    }
}*/

void startCancelWait(){
    operationCancelled = false;
    cancelWaitOnGoing =true;
//    IOCCPbits.IOCCP7 = 1; // For TIOL111x
    IOCCNbits.IOCCN7 = 1;
    IOCCFbits.IOCCF7 = 0;
    PIE0bits.IOCIE = 1;
    
}
    
void stopCancelWait(){
    PIE0bits.IOCIE = 0;
    // IOCCPbits.IOCCP7 = 0;    // For TIOL111x
    IOCCNbits.IOCCN7 = 0;
    cancelWaitOnGoing = false; 
}

void pauseCancelWait(){
    if(cancelWaitOnGoing){
    //    IOCCPbits.IOCCP7 = 0;    // For TIOL111x
        IOCCNbits.IOCCN7 = 0;
        PIE0bits.IOCIE = 0;
    }
}


void continueCancelWait(){
    __delay_ms(2);
    if(cancelWaitOnGoing){ 
        startCancelWait();
    }
  
}

/*
bool LIN_CANCEL(){
//    bool state = (unsigned char)GIE;
//    GIE = 0;
//    bool cancelled = operationCancelled;
//    GIE = state;
    // return (RC7_GetValue() == 0 || operationCancelled);
    return (RX1_GetValue() == 1 || operationCancelled);
    
}*/

void IOCC7_ISR(void)
{

    // clear the interrupt flag
    IOCCFbits.IOCCF7 = 0;
//    if(RC7_GetValue() == 0 && (0 == PIE3bits.TXIE)){ 
    if(RX1_GetValue() == 0 && (0 == PIE3bits.TX1IE)){   // For MCP2004
//    if(RX1_GetValue() == 1 && (0 == PIE3bits.TX1IE)){ // For TIOL111x
        operationCancelled = true;
    }

}
