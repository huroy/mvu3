/* 
 * File:   time.h
 * Author: onkila
 *
 * Created on September 13, 2018, 3:26 PM
 */

#ifndef TIME_H
#define	TIME_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include <stdint.h>
#include <stdbool.h>
#include "./mcc_generated_files/LINDrivers/lin_app.h"

bool delay_ms( uint16_t i );


#endif	/* TIME_H */

