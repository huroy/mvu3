#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/LINDrivers/lin_app.h"
#include <xc.h>
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/LINDrivers/lin_hardware.h"
#include "pressureadjust.h"
#include "valves.h"
#include <stdint.h>
// #include "board/mvuBoard1.h"
#include "math.h"
#include "time.h"

#define PRESSURE_SETTLE_TIME_INCREASE_MS 150
#define PRESSURE_SETTLE_TIME_INCREASE_BIG_CHANGE_MS 200
#define PRESSURE_SETTLE_TIME_INCREASE_FAST_MS 100
#define PRESSURE_SETTLE_TIME_DECREASE_MS 50
#define PRESSURE_SETTLE_TIME_VALVE_CLOSING_MS 200
#define PRESSURE_THRESHOLD_NOT_CHANGING 3
#define MAX_PRESSURE 1023

static bool pressureControl1(uint16_t pressure);
static bool pressureControl2(uint16_t pressure, uint8_t round);
static bool pressureControl3(uint16_t pressure, uint8_t round);
static bool adjust(uint16_t pressure);
static uint16_t correctPressure(bool increase, uint16_t currentPressure, uint16_t startPressure);

static void sendProgress(uint16_t targetPressure, uint16_t currentPressure, uint16_t startPressure);
// static uint16_t getMeasureDelayMs(uint16_t settingTimeMs, int round, int currentPressure);
// static void updateCoefficient(uint16_t time, int16_t pressureChange, int16_t wantedChange);

static int increaseValveSettleTimeMs = PRESSURE_SETTLE_TIME_INCREASE_MS;


bool pressureControl(uint16_t pressure, uint8_t controlType, uint8_t round){
    if(controlType == 1){
        return pressureControl1(pressure);
    } else if(controlType == 2){
        return pressureControl2(pressure, round);
    } else {
        return pressureControl3(pressure, round);
    }
}

static bool pressureControl1(uint16_t pressure) { // MIN D2

    currentPressure = getCurrentPressure();
    uint16_t startPressure = 0;
    float adjustProgress = 0;
    uint8_t low = currentPressure & 0xff;
    uint8_t high = (currentPressure >> 8);
    low = pressure & 0xff;
    high = (pressure >> 8);
    int pressureCompareCounter = 0;
    uint16_t sendPressureDataCounter = 0;
    int pressureDelta = 0;
    int requiredChange = 0;

    startPressure = currentPressure;

    if (pressure > (currentPressure + 1) && (currentPressure) < 1023) {

        __delay_ms(30);
        openValve(PRESSURE_INCREASE_VALVE);
        while ((ADCC_GetSingleScaledConversion(channel_VSENS0) < pressure) && !operationCancelled) {

            currentPressure = getCurrentPressure();
            __delay_ms(1);

            pressureCompareCounter++;
            sendPressureDataCounter++;
            if (pressureCompareCounter == 500) {
                pressureCompareCounter = 0;


                if (currentPressure >= lastPressure + 3) {
                    pressureTimeOut = 0;
                } else
                    pressureTimeOut++;
                lastPressure = currentPressure;
            }
            
            
            if (sendPressureDataCounter == 100) {
                sendPressureDataCounter = 0;
                sendProgress(finalPressureTarget, currentPressure, startPressure);                                           
            }

            if (pressureTimeOut > 5) {
                pressureTimeOut = 0;
                closeValve(PRESSURE_INCREASE_VALVE);
                adjustFailed=true;
                return true;
            }
            if (operationCancelled) {
                pressureTimeOut = 0;
                closeValve(PRESSURE_INCREASE_VALVE);
                
                return false;
            }


        }
        pressureTimeOut = 0;


        closeValve(PRESSURE_INCREASE_VALVE);
        adjustFailed=false;
        return true;
    }
    if (pressure < (currentPressure) && (currentPressure) > 0) {
        __delay_ms(30);
        openValve(PRESSURE_DECREASE_VALVE);
        while ((ADCC_GetSingleScaledConversion(channel_VSENS0) > pressure) && !operationCancelled) {

            currentPressure = getCurrentPressure();
            __delay_ms(1);

            pressureCompareCounter++;
            sendPressureDataCounter++;

            if (pressureCompareCounter == 500) {
                pressureCompareCounter = 0;

                if (currentPressure <= lastPressure - 3) {
                    pressureTimeOut = 0;
                } else
                    pressureTimeOut++;
                lastPressure = currentPressure;
            }
            
            if (sendPressureDataCounter > 100) {
                sendPressureDataCounter = 0;
                
                sendProgress(finalPressureTarget, currentPressure, startPressure);
            }



            if (pressureTimeOut > 5) {
                pressureTimeOut = 0;
                closeValve(PRESSURE_DECREASE_VALVE);
                return true;
            }
            if (operationCancelled) {
                pressureTimeOut = 0;
                closeValve(PRESSURE_DECREASE_VALVE);
                return false;
            }
        }
        pressureTimeOut = 0;
        closeValve(PRESSURE_DECREASE_VALVE);
        return true;
    }
    return true;
}



// #define AdGetPressureValue() ADCC_GetSingleScaledConversion(channel_VSENS0)

uint16_t getSettingTimeMs(uint16_t targetPressure, uint16_t currentPressure, int round);


static void sendProgress(uint16_t targetPressure, uint16_t currentPressure, uint16_t startPressure){
    int16_t pressureDelta = startPressure - currentPressure;
    int16_t requiredChange = startPressure - targetPressure;
    float adjustProgress = ((float) pressureDelta / (float) requiredChange)*100;
    pauseCancelWait();
    if(adjustProgress<101 && adjustProgress>0)
        EUSART1_Write((uint16_t) adjustProgress);
    __delay_ms(2);
    continueCancelWait();
}

//Used when pressure sensor is connected directly to increase valve output
static bool adjust(uint16_t pressure) {
    currentPressure = getCurrentPressure();
    uint16_t startPressure = currentPressure;   
    int16_t pressureDiff = pressure - currentPressure;
    uint16_t absDiff = abs_16(pressureDiff);
    uint16_t targetPressure = pressure;
    uint16_t previousPressure = currentPressure;
    uint16_t adjustTime = 0;
    bool targetReached = false;
    bool operationCompleted = false;
    uint16_t absPressureChange = 0;
    uint16_t correctedPressure = 0;
    bool increase = pressureDiff > 0;

    if(absDiff > 3){
        if(increase){
            openValve(PRESSURE_INCREASE_VALVE);
            delay_ms(increaseValveSettleTimeMs);         
        } else {
            openValve(PRESSURE_DECREASE_VALVE);
            delay_ms(PRESSURE_SETTLE_TIME_DECREASE_MS);
        }
        
        while (operationCompleted == false && !operationCancelled) {
            //currentPressure = AdGetPressureValue();
            currentPressure = ADCC_GetSingleScaledConversion(channel_VSENS0);
            correctedPressure = correctPressure(increase, currentPressure, startPressure );
            if(pressureDiff > 0){
                if(correctedPressure > targetPressure){
                    targetReached = true;
                    operationCompleted = true;
                }
            } else {
                if(correctedPressure < targetPressure){
                    targetReached = true;
                    operationCompleted = true;
                }
            }
            delay_ms(1);
            ++adjustTime;
            if(adjustTime % 100 == 0){
                sendProgress(targetPressure, correctedPressure, startPressure);
                if(adjustTime % 1000 == 0){
                    absPressureChange = abs_16(currentPressure - previousPressure);
                    previousPressure = currentPressure;
                    if(absPressureChange < PRESSURE_THRESHOLD_NOT_CHANGING){
                        operationCompleted = true;                    
                    }
                }
            }
        }
        closeValve(PRESSURE_INCREASE_VALVE);
        closeValve(PRESSURE_DECREASE_VALVE);
    } else {
        targetReached = true;
    }
    
    return targetReached;
    
}

static bool pressureControl2(uint16_t pressure, uint8_t round){
    uint8_t i=0;
    increaseValveSettleTimeMs = PRESSURE_SETTLE_TIME_INCREASE_MS;
    for(i=0;i<5;++i){
        if(adjust(pressure) == false){
            delay_ms(PRESSURE_SETTLE_TIME_VALVE_CLOSING_MS);
            return false;
        }
        delay_ms(PRESSURE_SETTLE_TIME_VALVE_CLOSING_MS);        
    }
    return true;   
}

static bool pressureControl3(uint16_t pressure, uint8_t round){
    uint8_t i=0;
    increaseValveSettleTimeMs = PRESSURE_SETTLE_TIME_INCREASE_BIG_CHANGE_MS;
    for(i=0;i<10;++i){
        if(adjust(pressure) == false){
            delay_ms(PRESSURE_SETTLE_TIME_VALVE_CLOSING_MS);
            return false;
        }
        delay_ms(PRESSURE_SETTLE_TIME_VALVE_CLOSING_MS);        
    }
    return true;   
}

uint16_t getCurrentPressure() {
    uint16_t currentPressureMean = 0;
    for (int i = 0; i < 20; i++) {
        currentPressureMean = currentPressureMean + ADCC_GetSingleScaledConversion(channel_VSENS0);
    }
    currentPressureMean = currentPressureMean / 20;
    return currentPressureMean;
}

#define MAX_CORRECTION_VALUE 100
static uint16_t correctPressure(bool increase, uint16_t currentPressure, uint16_t startPressure){
    uint16_t pressureDiff = 0;
    uint16_t correctedPressure = 0;
    uint16_t correctionValue = 0;
    if(increase){
        //Increasing disabled currently. We seem to achieve target without compensation
        //Problem in compensation is that we might adjust first too high and then too low
        //this might continue as long as we do adjustment and does not sound nice. Also it is possible
        //that we don't achieve target.
        /*pressureDiff = MAX_PRESSURE-currentPressure-ADCC_getAdcOffset();
        if(pressureDiff > 200){
            correctionValue = pressureDiff / 5 - 50;
        } */

    } else {
        if(currentPressure > 200){
            pressureDiff = currentPressure - 200;
            correctionValue = pressureDiff / 4;
        }
    }
    
    if(correctionValue > MAX_CORRECTION_VALUE){
        correctionValue = MAX_CORRECTION_VALUE;
    }
    if(increase){
        correctedPressure = currentPressure - correctionValue;
        if(correctedPressure < startPressure){
            correctedPressure = startPressure;
        }
    } else {
        correctedPressure = currentPressure + correctionValue;
        if(correctedPressure > startPressure){
            correctedPressure = startPressure;
        }
    }
    return correctedPressure;
}

