/* 
 * File:   full_bridge.h
 * Author: Tapio Yliniemi
 *
 * Created on November 3, 2021, 1:55 PM
 * Updated on 23.09.2022
 * 
 * This file specifies full-bridge output functions for motor driving.
 * The full-bridge output handler is called when PWM state change.
 * If PWM change is changed, CLC4 block generates interrupt for 
 * the full-bridge output handler. 
 * 
 * 
 */

#ifndef FULL_BRIDGE_H
#define	FULL_BRIDGE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "mcc_generated_files/mcc.h"
    
 
/* Absolute maximum current limit for normal operation */ 
#define BRIDGE_OVERCURRENT_HIGH_LIMIT       5000000     // microamperes = 5A
/* Overcurrent limit */
#define BRIDGE_OVERCURRENT_KICK_LIMIT       4000000     // microamperes = 4A
/* Overcurrent limit */
#define BRIDGE_OVERCURRENT_LOW_LIMIT        3000000     // microamperes = 3A
/* Current limit for normal operation */ 
#define BRIDGE_CURRENT_LIMIT                2500000     // microamperes  = 2.5A 
    
/* Values described with 500 us resolution. One PWM cycle is 500 us. */    
#define CURRENT_LIMIT_STEP          20      // 10 ms
#define OVERCURRENT_LIMIT_STEP      200     // 100 ms    
#define OVERCURRENT_COUNT           100     // 50 ms 

/*Limits for output stages duty cycle
 If duty cycle is out of these limits
 it may cause permanent damage od driver components
 due to the over temperature*/    
#define MAXIMUM_SAFETY_DUTYCYCLE   0x3CF   // 95% NOTE: This is the maximum duty cycle for the charge pump at output stage
#define MINIMUM_SAFETY_DUTYCYCLE   0x50
    
#define DEAD_TIME()     do { NOP();NOP();NOP(); } while(0)  // Delay ~600 ns

     
typedef enum {UP, DOWN} mDir_t;
typedef enum {NONE, bridge1, bridge2, bridge3} bridge_t;
typedef enum {NORMAL, OVERCURRENT, CURRENT_LIMIT} current_t;


bridge_t selectedOutputBridge;
mDir_t moveDirectionMotor;

current_t bridgeStatus;
uint8_t overCCount = 0;


void disableOutputs(void);
void enableOutputs(void);

void fullBridgeOutputHandler(void);
void fullBridgeInit(void);


#ifdef	__cplusplus
}
#endif

#endif	/* FULL_BRIDGE_H */

