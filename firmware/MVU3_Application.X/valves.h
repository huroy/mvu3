/* 
 * File:   valves.h
 * Author: Tapio Yliniemi
 *
 * Functions for controlling valves
 * 
 * Created on October 29, 2021
 * 
 */

#ifndef VALVES_H
#define	VALVES_H

#include <stdint.h>
#include <stdbool.h>


#ifdef	__cplusplus
extern "C" {
#endif

#define VALVE_NUMBER_MAX 8
    
#define PRESSURE_INCREASE_VALVE 1
#define PRESSURE_DECREASE_VALVE 2
#define MODE_VALVE_1 3
#define MODE_VALVE_2 4
#define MODE_VALVE_3 5
#define MODE_VALVE_4 6
#define MODE_VALVE_5 7
#define MODE_VALVE_6 8    

/**
* Open valve
* @param valveNumber   number of valve to open
*/
void openValve(uint8_t valveNumber);

/**
 * Close valve
 * @param valveNumber Number of valve to close
 */
void closeValve(uint8_t valveNumber);

/**
 * Set valve polarity (normally open (1) / closed (0)
 * Default 0x00
 * @param polarity
 */
void setValvePolarity(uint8_t polarity);

/*
 Stores status of all valves
 */
uint8_t valvestates[VALVE_NUMBER_MAX];

void valvesInit();
void completeValveInit(void);

void resetValveWatchDog();
void valveWatchDogHandlder();
void handleValveWatchDog(uint8_t valveNumber, bool watchDogEnabled);


#ifdef	__cplusplus
}
#endif

#endif	/* VALVES_H */