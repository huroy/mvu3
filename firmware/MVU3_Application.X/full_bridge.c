/* 
 * File:   full_bridge.h
 * Author: Tapio Yliniemi
 *
 * Created on November 3, 2021
 * 
 * This file specifies full-bridge output functions for motor driving.
 * 
 * 
 */

#include "full_bridge.h"
#include "motors.h"

bool outputsDisabled;


void fullBridgeInit(){
    
disableOutputs();
selectedOutputBridge=NONE;
// motorDutyValue = 0;
setMotorDutyValue(0);

}

void fullBridgeOutputHandler(){
    
    
    if(outputsDisabled) return;

    // Start the AD conversion for current measurement
    if(!CCP2CONbits.OUT && !ADCON0bits.ADGO){
        ADCON0bits.ADGO = 1;
    
    }
    
    if (moveDirectionMotor==DOWN){
        if(selectedOutputBridge==bridge1){
            HIN2_M1_SetOff();   // High side 2 of bridge1 must be allways off
            DEAD_TIME();    // Delay between output stage changes.
            LIN2_M1_SetOn();  // Low side 2 of bridge1 must be allways on
            if(CCP2CONbits.OUT){    
                // Set the bridge outputs to moving up state
                LIN1_M1_SetOff();   // Low side 1 of bridge1 off
                DEAD_TIME();    // Delay between output stage changes.
                HIN1_M1_SetOn();  // High side 1 of bridge1 on    
            }    
            else {
                // Set the bridge outputs to freewheeling state    
                HIN1_M1_SetOff();  // High side of bridge1 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN1_M1_SetOn();   // Low side of bridge1 on    
            }
        }
        if (selectedOutputBridge==bridge2){
            HIN2_M2_SetOff();   // High side 2 of bridge2 must be allways off
            DEAD_TIME();    // Delay between output stage changes.
            LIN2_M2_SetOn();  // Low side 2 of bridge2 must be allways on
            if(CCP2CONbits.OUT){  
            
                // Set the bridge outputs to moving up state
                LIN1_M2_SetOff();   // Low side 1 of bridge2 off
                DEAD_TIME();  // Delay between output stage changes.
                HIN1_M2_SetOn();  // High side 1 of bridge2 on    
            }    
            else {
                // Set the bridge outputs to freewheeling state    
                HIN1_M2_SetOff();  // High side 1 of bridge2 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN1_M2_SetOn();   // Low side 1 of bridge2 on    
            }
        }
        if (selectedOutputBridge==bridge3){
            HIN2_M3_SetOff();   // High side 2 of bridge3 must be allways off
            DEAD_TIME();    // Delay between output stage changes.
            LIN2_M3_SetOn();  // Low side 1 of bridge3  must be allways on
            if(CCP2CONbits.OUT){    
                // Set the bridge outputs to moving up state
                LIN1_M3_SetOff();   // Low side 1 of bridge3 off
                DEAD_TIME();  // Delay between output stage changes.
                HIN1_M3_SetOn();  // High side 1 of bridge3 on    
            }    
            else {
                // Set the bridge outputs to freewheeling state    
                HIN1_M3_SetOff();  // High side 1 of bridge3 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN1_M3_SetOn();   // Low side 1 of bridge3 on    
            }
        }
    }    
    else{        
        if(selectedOutputBridge==bridge1){
            HIN1_M1_SetOff();   // High side 1 of bridge1 must always be off
            DEAD_TIME();    // Delay between output stage changes.
            LIN1_M1_SetOn();  // Low side 1 of bridge1 must be allways on
            if(CCP2CONbits.OUT){    
                // Set the bridge outputs to moving up state
                LIN2_M1_SetOff();   // Low side 2 of bridge1 off
                DEAD_TIME();  // Delay between output stage changes.
                HIN2_M1_SetOn();  // High side 2 of bridge1 on    
            }    

            else {
                // Set the bridge outputs to freewheeling state    
                HIN2_M1_SetOff();  // High side 2 of bridge1 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN2_M1_SetOn();   // Low side 2 of bridge1 on    
            }
        }
        if (selectedOutputBridge==bridge2){
            HIN1_M2_SetOff();   // High side 1 of bridge2 must be allways off 
            DEAD_TIME();    // Delay between output stage changes.
            LIN1_M2_SetOn();  // Low side 1 of bridge2 must be allways on
            if(CCP2CONbits.OUT){    
                // Set the bridge outputs to moving up state
                LIN2_M2_SetOff();   // Low side 2 of bridge2 off
                DEAD_TIME();  // Delay between output stage changes.
                HIN2_M2_SetOn();  // High side 2 of bridge2 on    
            }    

            else {
                // Set the bridge outputs to freewheeling state    
                HIN2_M2_SetOff();  // High side 2 of bridge2 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN2_M2_SetOn();   // Low side 2 of bridge2 on    
            }
        }
        if (selectedOutputBridge==bridge3){
            HIN1_M3_SetOff();   // High side 1 of bridge3 must be allways off 
            DEAD_TIME();    // Delay between output stage changes.
            LIN1_M3_SetOn();  // Low side 1 of bridge3 must be allways on
            if(CCP2CONbits.OUT){    
            // Set the bridge outputs to moving up state
                LIN2_M3_SetOff();   // Low side 2 of bridge3 off
                DEAD_TIME();  // Delay between output stage changes.
                HIN2_M3_SetOn();  // High side 2 of bridge3 on    
            }    

            else {
                // Set the bridge outputs to freewheeling state    
                HIN2_M3_SetOff();  // High side 2 of bridge3 off
                DEAD_TIME();  // Delay between output stage changes.
                LIN2_M3_SetOn();   // Low side 2 of bridge3 on    
            }
        }    
    }        

}

void enableOutputs(void){
    
    HIN1_M1_SetOff();
    LIN1_M1_SetOff();
    HIN2_M1_SetOff();
    LIN2_M1_SetOff();

    HIN1_M2_SetOff();
    LIN1_M2_SetOff();
    HIN2_M2_SetOff();
    LIN2_M2_SetOff();

    HIN1_M3_SetOff();
    LIN1_M3_SetOff();
    HIN2_M3_SetOff();
    LIN2_M3_SetOff();
       
    // select the A/D channel
    if (selectedOutputBridge == bridge1){
        ADPCH = channel_SENSEM1;
    }
    if (selectedOutputBridge == bridge2){
        ADPCH = channel_SENSEM2;
    }
    if (selectedOutputBridge == bridge3){
        ADPCH = channel_SENSEM3;
    }

    /*Enable AD conversion for single conversion with interrupts*/
    ADCON0bits.ADCONT = 0;  // Single conversion mode 
    PIR1bits.ADIF = 0;      // Clear the ADC interrupt flag
    PIE1bits.ADIE = 1;      // Enabling ADCC interrupt.
    ADCON0bits.ADON = 1;    // Turn on the ADC module
    
    outputsDisabled = false; 
    kickStart = true;
}
void disableOutputs(void){

    HIN1_M1_SetOff();
    LIN1_M1_SetOff();
    HIN2_M1_SetOff();
    LIN2_M1_SetOff();

    HIN1_M2_SetOff();
    LIN1_M2_SetOff();
    HIN2_M2_SetOff();
    LIN2_M2_SetOff();

    HIN1_M3_SetOff();
    LIN1_M3_SetOff();
    HIN2_M3_SetOff();
    LIN2_M3_SetOff();
    
    selectedOutputBridge=NONE;
    
    /*Disable AD conversion */
    PIE1bits.ADIE = 0;      // Disable ADCC interrupt.
    ADCON0bits.ADGO = 0;    // Stop AD conversion
    ADCON0bits.ADON = 0;    // Turn off the ADC module
    kickStart = false;
    outputsDisabled = true;
    bridgeStatus = NORMAL;
    overCCount = 0;
}