#include <xc.h>
#include "mvu.h"
#include "valves.h"
#include "motors.h"
#include "eeprom_addresses.h"
#include "mcc_generated_files/mcc.h"

/*
 MVUBoot is called when MVU is powered, this function will release 
 * pressure and drive motors to 0 position.
 * FACTORY SETUP NEED TO BE DONE FIRST
 * 
 * This function needs to move motors and measure motor currents 
 * so therefore interrupts must be enable at first.
 */
void MVUBoot() {
    
    
    /* When using interrupts, you need to set the Global and 
       Peripheral Interrupt Enable bits */
    
    /* Use the following macros to: */

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    OK_LED_SetHigh(); // Set Status Led 1 to on state
    
    
    int setupIsDone = EEPROM_READ(ADDRESS_SETUP_DONE);
    valvesInit();
    
    if (setupIsDone == 1) {
        motorsMoveDown();
    }
}
