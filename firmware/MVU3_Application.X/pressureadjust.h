/* 
 * File:   pressureadjust.h
 * Author: OMISTAJ
 *
 * Created on 11. tammikuuta 2018, 9:41
 */

#ifndef PRESSUREADJUST_H
#define	PRESSUREADJUST_H

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif


/**
 * Control pressure
 * ControlType 1:Normal pressure controlling function
 * Used when there is no peaks in measured pressure
 * ControlType 2:Special pressure controlling function
 * Used when increase/decrease valve can not be open all the time
 * @param pressure Wanted pressure
 * @return True, if adjustment was succesful
 */
bool pressureControl(uint16_t pressure, uint8_t controlType, uint8_t round);

/**
 * Get current pressure 
 * @return pressure
 */

uint16_t getCurrentPressure();

void initPressureControl();


uint16_t pressure = 269;
uint16_t currentPressure;

uint16_t height = 0;
uint16_t currentHeight = 0;
uint16_t totalTicks = 0;
uint16_t counter = 0;
uint16_t pressureTimeOut = 0;
uint16_t motorTimeOut = 0;
uint16_t pressureTimeOutCheck = 0;
uint16_t lastPressure = 0;
uint16_t finalPressureTarget=0;

#endif	/* PRESSUREADJUST_H */